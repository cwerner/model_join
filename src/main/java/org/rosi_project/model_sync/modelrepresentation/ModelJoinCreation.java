package org.rosi_project.model_sync.modelrepresentation;

import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.attributes;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.outgoing;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.supertype;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.subtype;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.naturalJoin;

import java.io.File;

import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.writer.*;

/**
 * Create different ModelJoin Queries.
 * 
 * @author Christopher
 */
public class ModelJoinCreation {

	public static void main(String[] args) {		
		ModelJoinCreation mjc = new ModelJoinCreation();
		//mjc.createModelJoinQueriesForSimpleLibrary();
		mjc.createModelJoinQueriesForShrinkingEclipseLibrary();		
	}
	
	public void createModelJoinQueriesForSimpleLibrary () {
		ClassResource library = ClassResource.from("lib", "Library");
	    ClassResource employee = ClassResource.from("lib", "Employee");
	    ClassResource person = ClassResource.from("lib", "Person");

	    AttributePath libraryName = AttributePath.from(library, "name");
	    AttributePath libraryEmployees = AttributePath.from(library, "employees");
	    AttributePath employeeSalary = AttributePath.from(employee, "salary");
	    AttributePath employeeManager = AttributePath.from(employee, "manager");
	    AttributePath personName = AttributePath.from(person, "name");
	    
	    ModelJoinExpression mjManager = ModelJoinBuilder.createNewModelJoin()
		        .add(naturalJoin()
		            .join(employee)
		            .with(employee)
		            .as(employee)
		            .keep(attributes(employeeSalary))
		            .keep(outgoing(employeeManager)
		                .as(employee)
		                .buildExpression()
		            )
		            .keep(supertype(person)
			                .as(person)
			                .keep(attributes(personName))
			                .buildExpression()
			        )
		            .done())
		        .build();
	    
		ModelJoinExpression mjComplete = ModelJoinBuilder.createNewModelJoin()
		        .add(naturalJoin()
		            .join(library)
		            .with(library)
		            .as(library)
		            .keep(attributes(libraryName))
		            .keep(outgoing(libraryEmployees)
		                .as(employee)
		                .keep(attributes(employeeSalary))
		                .keep(outgoing(employeeManager)
		                		.as(employee)
		                		.buildExpression()
		                )
		                .keep(supertype(person)
				                .as(person)
				                .keep(attributes(personName))
				                .buildExpression()
				        )
		                .buildExpression()
		            )
		            .done())
		        .build();
		
		ModelJoinExpression mjSimple = ModelJoinBuilder.createNewModelJoin()
		        .add(naturalJoin()
		            .join(library)
		            .with(library)
		            .as(library)
		            .keep(attributes(libraryName))
		            .keep(outgoing(libraryEmployees)
		                .as(employee)
		                .keep(attributes(employeeSalary))
		                .keep(supertype(person)
				                .as(person)
				                .keep(attributes(personName))
				                .buildExpression()
				        )
		                .buildExpression()
		            )
		            .done())
		        .build();
		
		File fileComplete = new File("libraryComplete.modeljoin");
		File fileSimple = new File("librarySimple.modeljoin");
		File fileManager = new File("manager.modeljoin");

	    FileBasedModelJoinWriter writerComplete = new FileBasedModelJoinWriter(fileComplete);
	    writerComplete.write(mjComplete);
	    
	    FileBasedModelJoinWriter writerSimple = new FileBasedModelJoinWriter(fileSimple);
	    writerSimple.write(mjSimple);
	    
	    FileBasedModelJoinWriter writerManager = new FileBasedModelJoinWriter(fileManager);
	    writerManager.write(mjManager);
	}
	
	public void createModelJoinQueriesForShrinkingEclipseLibrary () {
		ClassResource library = ClassResource.from("eclipse", "Library");
	    ClassResource book = ClassResource.from("eclipse", "Book");
	    ClassResource circulatingItem = ClassResource.from("eclipse", "CirculatingItem");	    
	    ClassResource item = ClassResource.from("eclipse", "Item");

	    AttributePath libraryName = AttributePath.from(library, "name");
	    AttributePath libraryStock = AttributePath.from(library, "stock");	    
	    AttributePath itemTitle = AttributePath.from(item, "title");	    
	    AttributePath bookPages = AttributePath.from(book, "pages");
	    	    
	    ModelJoinExpression mj = ModelJoinBuilder.createNewModelJoin()
		        .add(naturalJoin()
		            .join(library)
		            .with(library)
		            .as(library)
		            .keep(attributes(libraryName))
		            .keep(outgoing(libraryStock)
		                .as(circulatingItem)		                
		                .keep(attributes(itemTitle))
		                .keep(subtype(book)
		                		.as(book)
		                		.keep(attributes(bookPages))
				                .buildExpression())
		                .buildExpression()
		            )
		            .done())
		        .build();
	    
	    File file = new File("shrinkingEclipse.modeljoin");

	    FileBasedModelJoinWriter writer = new FileBasedModelJoinWriter(file);
	    writer.write(mj);
	}

}
