package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.util.Assert;

/**
 * A {@code KeepSubTypeExpression} instructs the {@code ModelJoin} runtime to instantiate the most
 * specific type that inherits from this type as well. The additional instance will be built
 * according to the {@code KeepExpression keep statements} that it is constructed of.
 * <p>
 * In order to prevent ambiguities a {@code KeepSubTypeExpression} may only be specified on joins
 * that do not participate in another {@link JoinExpression join statement}.
 *
 * @author Rico Bergmann
 */
public class KeepSubTypeExpression extends CompoundKeepExpression {

  /**
   * The {@code KeepSubTypeBuilder} enables the construction of new {@code KeepSubTypeExpression}
   * instances through a nice and fluent interface.
   *
   * @author Rico Bergmann
   */
  public static class KeepSubTypeBuilder extends CompoundKeepBuilder {

    private ClassResource typeToKeep;

    /**
     * Default constructor.
     */
    private KeepSubTypeBuilder() {
      this.keeps = new ArrayList<>();
    }

    /**
     * Specifies the subtype that should be instantiated.
     */
    @Nonnull
    public KeepSubTypeBuilder subtype(@Nonnull ClassResource type) {
      this.typeToKeep = type;
      return this;
    }    

    /**
     * Finishes the construction process.
     */
    @Nonnull
    public KeepSubTypeExpression buildExpression() {
      Assert.noNullArguments("All components must be specified", typeToKeep, target, keeps);
      return new KeepSubTypeExpression(typeToKeep, target, keeps);
    }

  }

  /**
   * Starts the creation process for a new {@code KeepSubTypeExpression}.
   */
  @Nonnull
  public static KeepSubTypeBuilder keep() {
    return new KeepSubTypeBuilder();
  }

  @Nonnull
  private final ClassResource typeToKeep;
  
  @Nonnull
  private final ClassResource target;

  /**
   * Full constructor.
   *
   * @param typeToKeep the subtype that should be instantiated.
   * @param target the name of the view that should be generated for all matching instances
   * @param keeps the keep statements that should form the attributes of the generated view
   *     instances
   */
  public KeepSubTypeExpression(
      @Nonnull ClassResource typeToKeep,
      @Nonnull ClassResource target,
      @Nonnull List<KeepExpression> keeps) {
    this.typeToKeep = typeToKeep;
    this.target = target;
    this.keeps = keeps;
  }

  /**
   * Provides the subtype that should be instantiated.
   */
  @Nonnull
  public ClassResource getType() {
    return typeToKeep;
  }

  /**
   * Provides the name of the view that should be generated for all matching instances.
   */
  @Nonnull
  public ClassResource getTarget() {
    return target;
  }

  @Override
  public void accept(@Nonnull KeepExpressionVisitor visitor) {
    visitor.visit(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeepSubTypeExpression that = (KeepSubTypeExpression) o;
    return typeToKeep.equals(that.typeToKeep) &&
        target.equals(that.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(typeToKeep, target);
  }

  @Override
  public String toString() {
    return "keep subtype " + typeToKeep + " as " + target;
  }
}
