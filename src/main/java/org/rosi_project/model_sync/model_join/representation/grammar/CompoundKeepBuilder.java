package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.List;

import javax.annotation.Nonnull;

import org.rosi_project.model_sync.model_join.representation.core.ClassResource;

public abstract class CompoundKeepBuilder {

	protected ClassResource target;
	@Nonnull
	protected List<KeepExpression> keeps;
	
	/**
     * Specifies the name of the class that should be generated for the subtype instances.
     */
    @Nonnull
    public CompoundKeepBuilder as(@Nonnull ClassResource target) {
      this.target = target;
      return this;
    }

    /**
     * Adds a {@link KeepExpression keep statement} for the subtype-class. The described attribute
     * will be initialized for each instantiated subtype element.
     */
    @Nonnull
    public CompoundKeepBuilder keep(@Nonnull KeepExpression keep) {
      this.keeps.add(keep);
      return this;
    }
    
    @Nonnull
    public abstract KeepExpression buildExpression();

}
