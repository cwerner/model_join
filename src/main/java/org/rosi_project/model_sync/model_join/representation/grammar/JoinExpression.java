package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;

/**
 * A {@code JoinExpression} combines two classes of the source models and
 * generates a new view derived from them.
 * <p>
 * At a conceptual level, these classes are referred to as the {@code left} and
 * {@code right} class.
 * <p>
 * Other than the classes to combine, each join also consists of a number of
 * {@link KeepExpression}s which contain the fields which should form the
 * resulting class as well as information on how to set their values.
 * <p>
 * As there are a number of different joins available, this class is left
 * {@code abstract} with subclasses to represent the specific joins.
 *
 * @author Rico Bergmann
 * @see NaturalJoinExpression
 * @see OuterJoinExpression
 * @see ThetaJoinExpression
 */
public abstract class JoinExpression implements Iterable<KeepExpression> {

	/**
	 * The {@code JoinType} defines which rules should be applied when
	 * performing the join.
	 */
	public enum JoinType {

		/**
		 * The {@code natural join} combines two classes based on attributes
		 * with equal name and type.
		 *
		 * @see NaturalJoinExpression
		 */
		NATURAL,

		/**
		 * The {@code outer join} works like the {@link #NATURAL} one, but
		 * leaves instances from one class with no corresponding instance in the
		 * other class according to the {@code outer join
		 * type}. See the subclass for details.
		 *
		 * @see OuterJoinExpression
		 */
		OUTER,

		/**
		 * The {@code theta join} is more general than the {@link #NATURAL} and
		 * {@link #OUTER} one as it enables an arbitrary criteria to define
		 * whether two instances are "joinable" or not.
		 *
		 * @see ThetaJoinExpression
		 */
		THETA
	}

	@Nonnull
	protected final ClassResource left;

	@Nonnull
	protected final ClassResource right;

	@Nonnull
	protected final ClassResource target;

	@Nonnull
	protected final List<KeepExpression> keeps;

	/**
	 * Full constructor.
	 *
	 * @param left
	 *            the left class to use in the join
	 * @param right
	 *            the right class to use in the join
	 * @param target
	 *            the name of the resulting view
	 * @param keeps
	 *            the keep statements in the join
	 */
	protected JoinExpression(@Nonnull ClassResource left, @Nonnull ClassResource right, @Nonnull ClassResource target,
			@Nonnull List<KeepExpression> keeps) {

		this.left = left;
		this.right = right;
		this.target = target;
		this.keeps = keeps;
	}

	/**
	 * Proof if the right and the left class path are equals.
	 * @return result value
	 */
	public boolean isSameElement() {
		if (left.getResourceName().equals(right.getResourceName())
				&& left.getResourcePath().equals(right.getResourcePath())) {
			return true;
		}
		return false;
	}

	/**
	 * Provides the based class used in {@code this} join. Only important for
	 * outer join in other cases always Left.
	 */
	@Nonnull
	public ClassResource getBaseModel() {
		return this.getLeft();
	}

	/**
	 * Provides the other class used in {@code this} join. Only important for
	 * outer join in other cases always Right.
	 */
	@Nonnull
	public ClassResource getOtherModel() {
		return this.getRight();
	}

	/**
	 * Provides the left class used in {@code this} join.
	 */
	@Nonnull
	public ClassResource getLeft() {
		return left;
	}

	/**
	 * Provides the right class used in {@code this} join.
	 */
	@Nonnull
	public ClassResource getRight() {
		return right;
	}

	/**
	 * Provides the name of the class resulting from {@code this} join.
	 */
	@Nonnull
	public ClassResource getTarget() {
		return target;
	}

	/**
	 * Provides the {@code keep} statements that should be used to build the
	 * resulting class.
	 */
	@Nonnull
	public Iterable<KeepExpression> getKeeps() {
		return keeps;
	}

	public List<KeepExpression> getKeepsList() {
		return Collections.unmodifiableList(keeps);
	}

	/**
	 * Provides the type of {@code this} join.
	 */
	@Nonnull
	public abstract JoinType getType();

	/**
	 * Provides the type of {@code this} join as a non-technical {@code String}.
	 */
	@Nonnull
	abstract String getJoinTypeAsString();

	@Nonnull
	@Override
	public Iterator<KeepExpression> iterator() {
		return keeps.iterator();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		JoinExpression that = (JoinExpression) o;
		return this.target.equals(that.target);
	}

	@Override
	public int hashCode() {
		return Objects.hash(target);
	}

	@Override
	public String toString() {
		return getJoinTypeAsString() + " " + left.toString() + " with " + right.toString() + " as " + target.toString();
	}
}
