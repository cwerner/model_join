package org.rosi_project.model_sync.model_join.representation.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;

/**
 * The {@code FileBasedModelJoinWriter} outputs a {@link ModelJoinExpression model join description}
 * onto the File System.
 *
 * @author Rico Bergmann
 */
public class FileBasedModelJoinWriter implements ModelJoinWritingService<Boolean> {

  private final File outputFile;
  private final StringBasedModelJoinWriter toStringWriter;

  /**
   * Full constructor.
   *
   * @param outputFile the file to write to
   */
  public FileBasedModelJoinWriter(@Nonnull File outputFile) {
    this.outputFile = outputFile;
    this.toStringWriter = StringBasedModelJoinWriter.withNewlines();
  }

  /**
   * Writes the given description to the {@code outputFile}.
   *
   * @return whether the file was successfully written
   */
  @Override
  public Boolean write(@Nonnull ModelJoinExpression modelJoin) {
    try (BufferedWriter writer = Files.newBufferedWriter(
        outputFile.toPath(),
        StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)) {

      String modelJoinTextRepresentation = toStringWriter.write(modelJoin);
      writer.write(modelJoinTextRepresentation);

      return true;
    } catch (IOException ioe) {
      return false;
    }
  }
}
