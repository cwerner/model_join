package org.rosi_project.model_sync.model_join.representation.util;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.OCLConstraint;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.NaturalJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.OuterJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.OuterJoinExpression.JoinDirection;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.grammar.ThetaJoinExpression;

/**
 * The {@code JoinFactory} should be used as the main facility to create new {@link JoinExpression}
 * instances. It supports instantiation for all subclasses and provides a fluid and natural
 * interface to set them up.
 *
 * @author Rico Bergmann
 */
public class JoinFactory {

  /**
   * Starts the construction process for a new join.
   */
  @Nonnull
  public static JoinFactory createNew() {
    return new JoinFactory();
  }

  private JoinFactory() {
    // pass
  }

  /**
   * Constructs a new {@link OuterJoinExpression outer join}
   */
  @Nonnull
  public OuterJoinBuilder outer() {
    return new OuterJoinBuilder();
  }

  /**
   * Constructs a new {@link NaturalJoinExpression natural join}.
   */
  @Nonnull
  public NaturalJoinBuilder natural() {
    return new NaturalJoinBuilder();
  }

  /**
   * Constructs a new {@link ThetaJoinExpression theta join}.
   */
  @Nonnull
  public ThetaJoinBuilder theta() {
    return new ThetaJoinBuilder();
  }

  /**
   * This is the base for all actual join builders and contains the fields that have to be present
   * in each join. Consult the documentation of the {@link JoinExpression} class for details on
   * those fields.
   * <p>
   * Despite all builders working very similarly, they may not be refactored to have more logic
   * provided through this class due to limitations of the Java language.
   *
   * @see JoinExpression
   */
  public static abstract class AbstractJoinBuilder {
    protected ClassResource left;
    protected ClassResource right;
    protected ClassResource target;
    protected List<KeepExpression> keeps = new ArrayList<>();
    
    /**
     * Specifies the left source.
     */
    @Nonnull
    public AbstractJoinBuilder join(@Nonnull ClassResource left) {
      this.left = left;
      return this;
    }

    /**
     * Specifies the right source.
     */
    @Nonnull
    public AbstractJoinBuilder with(@Nonnull ClassResource right) {
      this.right = right;
      return this;
    }

    /**
     * Specifies the name of the resulting class.
     */
    @Nonnull
    public AbstractJoinBuilder as(@Nonnull ClassResource target) {
      this.target = target;
      return this;
    }

    /**
     * Specifies a field that should be contained in the resulting view.
     */
    @Nonnull
    public AbstractJoinBuilder keep(@Nonnull KeepExpression keep) {
      this.keeps.add(keep);
      return this;
    }
    
    public abstract JoinExpression done();
  }

  /**
   * The {@code OuterJoinBuilder} takes care of setting up new {@link OuterJoinExpression}
   * instances.
   *
   * @see OuterJoinExpression
   */
  public static class OuterJoinBuilder extends AbstractJoinBuilder {

    private OuterJoinExpression.JoinDirection direction;

    /**
     * Default constructor.
     * <p>
     * {@code private} to prevent direct instantiation.
     */
    private OuterJoinBuilder() {
      // pass
    }

    /**
     * Configures the join to use the right source model as the primary one.
     */
    @Nonnull
    public OuterJoinBuilder rightOuter() {
      direction = JoinDirection.RIGHT;
      return this;
    }

    /**
     * Configures the join to use the left source model as the primary one.
     */
    @Nonnull
    public OuterJoinBuilder leftOuter() {
      direction = JoinDirection.LEFT;
      return this;
    }    

    /**
     * Finishes the construction process and provides the resulting {@code join}.
     */
    @Nonnull
    public OuterJoinExpression done() {
      return new OuterJoinExpression(left, right, target, direction, keeps);
    }
  }

  /**
   * The {@code NaturalJoinBuilder} takes care of setting up new {@link NaturalJoinExpression}
   * instances.
   *
   * @see NaturalJoinExpression
   */
  public static class NaturalJoinBuilder extends AbstractJoinBuilder {

    /**
     * Default constructor.
     * <p>
     * {@code private} to prevent direct instantiation.
     */
    private NaturalJoinBuilder() {
      // pass
    }    

    /**
     * Finishes the construction process and provides the resulting {@code join}.
     */
    @Nonnull
    public NaturalJoinExpression done() {
      return new NaturalJoinExpression(left, right, target, keeps);
    }
  }

  /**
   * The {@code ThetaJoinBuilder} takes care of setting up new {@link ThetaJoinExpression}
   * instances.
   *
   * @see ThetaJoinExpression
   */
  public static class ThetaJoinBuilder extends AbstractJoinBuilder {

    private OCLConstraint condition;

    /**
     * Default constructor.
     * <p>
     * {@code private} to prevent direct instantiation.
     */
    private ThetaJoinBuilder() {
      // pass
    }    

    /**
     * Specifies the join condition.
     */
    @Nonnull
    public ThetaJoinBuilder where(@Nonnull OCLConstraint condition) {
      this.condition = condition;
      return this;
    }

    /**
     * Specifies the join condition.
     */
    @Nonnull
    public ThetaJoinBuilder where(@Nonnull String oclCondition) {
      return where(OCLConstraint.of(oclCondition));
    }

    /**
     * Finishes the construction process and provides the resulting {@code join}.
     */
    @Nonnull
    public ThetaJoinExpression done() {
      return new ThetaJoinExpression(left, right, target, condition, keeps);
    }
  }

}
