package org.rosi_project.model_sync.model_join.representation.core;

import javax.annotation.Nonnull;

/**
 * An {@code OCLConstraint} specifies some predicate on (possibly multiple) models that have to hold
 * under certain conditions.
 * <p>
 * This class is a specialization of an arbitrary {@link OCLStatement} for statements, that
 * evaluate to a boolean values.
 *
 * @author Rico Bergmann
 */
public class OCLConstraint extends OCLStatement {

  /**
   * Generates a new constraint with the given content.
   */
  @Nonnull
  public static OCLConstraint of(@Nonnull String content) {
    return new OCLConstraint(content);
  }

  /**
   * Full constructor.
   *
   * @param content the actual constraint
   */
  public OCLConstraint(@Nonnull String content) {
    super(content);
  }
}
