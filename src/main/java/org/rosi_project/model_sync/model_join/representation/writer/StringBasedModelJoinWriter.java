package org.rosi_project.model_sync.model_join.representation.writer;

import java.util.StringJoiner;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.grammar.CompoundKeepExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAggregateExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAttributesExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepCalculatedExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepReferenceExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSubTypeExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSuperTypeExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.NaturalJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.OuterJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.ThetaJoinExpression;
import org.rosi_project.model_sync.model_join.representation.util.Functional;
import org.rosi_project.model_sync.model_join.representation.util.Nothing;

/**
 * The {@code StringBasedModelJoinWriter} transforms a {@link ModelJoinExpression model join description}
 * to a single {@code String}.
 *
 * @author Rico Bergmann
 */
class StringBasedModelJoinWriter implements ModelJoinWritingService<String> {

  private static final String NEWLINE_DELIM = "\n";
  private static final String EMPTY_DELIM = " ";
  private static final String DEFAULT_INDENTATION = "  ";

  static StringBasedModelJoinWriter createDefault() {
    return new StringBasedModelJoinWriter(false);
  }

  static StringBasedModelJoinWriter withNewlines() {
    return new StringBasedModelJoinWriter(true);
  }

  private final String delimiter;
  private final boolean useNewlineDelimiters;

  private String currentIndentation = "";

  private StringBasedModelJoinWriter(boolean useNewlinesDelimiters) {
    if (useNewlinesDelimiters) {
      this.delimiter = NEWLINE_DELIM;
    } else {
      this.delimiter = EMPTY_DELIM;
    }
    this.useNewlineDelimiters = useNewlinesDelimiters;
  }

  @Override
  public String write(@Nonnull ModelJoinExpression modelJoin) {
    StringJoiner joiner = new StringJoiner(delimiter + delimiter);

    modelJoin.getJoins().forEach(
        joinStatement -> joiner.merge(writeJoin(joinStatement))
    );

    return joiner.toString();
  }

  private StringJoiner writeJoin(@Nonnull JoinExpression join) {
    StringJoiner joiner = new StringJoiner(" ");

    String joinHeader = Functional.<String>match(join)
        .caseOf(ThetaJoinExpression.class, "theta join")
        .caseOf(NaturalJoinExpression.class, "natural join")
        .caseOf(OuterJoinExpression.class, outerJoin -> {
          switch (outerJoin.getDirection()) {
            case LEFT:
              return "left outer join";
            case RIGHT:
              return "right outer join";
            default:
              throw new AssertionError(outerJoin.getDirection());
          }
        })
        .runOrThrow();
    joiner.add(String.format(currentIndentation + "%s %s with %s as %s", joinHeader, join.getLeft(), join.getRight(), join.getTarget()));
    Functional.<Nothing>match(join)
        .caseOf(ThetaJoinExpression.class, thetaJoin -> {
          joiner.add("where").add(thetaJoin.getCondition().toString());
          return Nothing.__();
        })
        .defaultCase(Nothing.__())
        .runOrThrow();

    joiner.add("{\n");
    increaseIndentationIfNecessary();
    join.getKeeps().forEach(keep -> joiner.add(currentIndentation + writeKeep(keep) + delimiter));
    decreaseIndentationIfNecessary();
    joiner.add(currentIndentation + "}");

    return joiner;
  }

  private String writeKeep(KeepExpression keep) {

    return Functional.<String>match(keep)
        .caseOf(KeepAttributesExpression.class, keepAttributes -> {
          StringJoiner attributesJoiner = new StringJoiner(", ");
          keepAttributes.getAttributes().forEach(attr -> attributesJoiner.add(attr.toString()));
          return "keep attributes " + attributesJoiner;
        })
        .caseOf(KeepCalculatedExpression.class, keepCalculated ->
            "keep calculated attribute "
                + keepCalculated.getCalculationRule()
                + " as " + keepCalculated.getTarget())
        .caseOf(KeepAggregateExpression.class, keepAggregate ->
            String.format("keep aggregate %s(%s) over %s as %s",
                keepAggregate.getAggregation().name().toLowerCase(),
                keepAggregate.getAggregatedAttribute(),
                keepAggregate.getSource(), keepAggregate.getTarget())
            )
        .caseOf(KeepReferenceExpression.class, keepRef -> {
          String header = String.format("keep %s %s as type %s {" + delimiter, keepRef.getReferenceDirection().name().toLowerCase(), keepRef.getAttribute(), keepRef.getTarget());
          return writeCompound(keepRef, header);
        })
        .caseOf(KeepSubTypeExpression.class, keepSubtype -> {
          String header = String.format("keep subtype %s as type %s {" + delimiter, keepSubtype.getType(), keepSubtype.getTarget());
          return writeCompound(keepSubtype, header);
        })
        .caseOf(KeepSuperTypeExpression.class, keepSupertype -> {
          String header = String.format("keep supertype %s as type %s {" + delimiter, keepSupertype.getType(), keepSupertype.getTarget());
          return writeCompound(keepSupertype, header);
        })
        .runOrThrow();

  }

  private String writeCompound(CompoundKeepExpression compoundKeep, String header) {
    StringJoiner refJoiner = new StringJoiner(" ");

    refJoiner.add(header);
    increaseIndentationIfNecessary();
    compoundKeep.getKeeps().forEach(keep -> refJoiner.add(currentIndentation + writeKeep(keep) + delimiter));
    decreaseIndentationIfNecessary();
    refJoiner.add(currentIndentation + "}");

    return refJoiner.toString();
  }

  private void increaseIndentationIfNecessary() {
    if (useNewlineDelimiters) {
      currentIndentation += DEFAULT_INDENTATION;
    }
  }

  private  void decreaseIndentationIfNecessary() {
    if (useNewlineDelimiters) {
      int deletionIdx = currentIndentation.lastIndexOf(DEFAULT_INDENTATION);
      currentIndentation = currentIndentation.substring(0, deletionIdx);
    }
  }

}
