package org.rosi_project.model_sync.model_join.representation.parser;

import java.io.File;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;

/**
 * The {@code ModelJoinParser} reads a {@code ModelJoin} file from disk and creates a matching
 * logical representation for it.
 *
 * @author Rico Bergmann
 */
public interface ModelJoinParser {

  /**
   * Converts the {@code ModelJoin} file into a {@link ModelJoinExpression}, wrapped into an {@code
   * Optional}. If parsing fails, an empty {@code Optional} will be returned.
   */
  @Nonnull
  Optional<ModelJoinExpression> read(@Nonnull File modelFile);

  /**
   * Converts the {@code ModelJoin} file into a {@link ModelJoinExpression}. If parsing fails, a
   * {@link ModelJoinParsingException} will be thrown.
   *
   * @throws ModelJoinParsingException if the model may not be parsed for some reason.
   */
  @Nonnull
  ModelJoinExpression readOrThrow(@Nonnull File modelFile);

}
