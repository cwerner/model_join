package org.rosi_project.model_sync.model_join.representation.core;

import javax.annotation.Nonnull;

/**
 * A {@code ClassResource} represents a fully-qualified reference to some class to be used in a
 * join. This class merely exist to emphasize that some resource actually represents a class and
 * thus provides a nicer (i.e. better named) interface to query for the resource's fields.
 * <p>
 * In difference to a {@link ResourcePath}, instances of this class will drop the {@code .java}
 * ending of the file name if referenced from the file system.
 *
 * @author Rico Bergmann
 */
public class ClassResource extends ResourcePath {

  /**
   * Generates a new {@code path} given the path to its containing <em>package</em> and the name of
   * the class which should be referenced.
   */
  @Nonnull
  public static ClassResource from(@Nonnull String packagePath, @Nonnull String className) {
    return new ClassResource(packagePath, className);
  }

  /**
   * @see ResourcePath#parse(String)
   */
  @Nonnull
  public static ClassResource fromQualifiedName(@Nonnull String qualifiedClass) {
    /*
     * A class resource is just a better representation of a class (in difference to just _some_
     * kind of resource) but works just the same - thus we will re-use all the parsing logic from
     * the ResourcePath
     */
    ResourcePath parsedResource = ResourcePath.parse(qualifiedClass);
    return new ClassResource(parsedResource.resourcePath, parsedResource.resourceName);
  }

  /**
   * @see ResourcePath#parseAsFileSystemPath(String)
   */
  @Nonnull
  public static ClassResource fromFileSystem(@Nonnull String fullPath) {
    /*
     * A class resource is just a better representation of a class (in difference to just _some_
     * kind of resource) but works just the same - thus we will re-use all the parsing logic from
     * the ResourcePath
     */
    ResourcePath parsedResource = ResourcePath.parseAsFileSystemPath(dropFileTypeEnding(fullPath));
    return new ClassResource(parsedResource.resourcePath, parsedResource.resourceName);
  }

  /**
   * Deletes the {@code .java} ending from a resource name.
   */
  private static String dropFileTypeEnding(String fullPath) {
    return fullPath.replaceAll("\\.java$", "");
  }

  /**
   * Full constructor.
   *
   * @param containingPackage the path to the package to which the class belongs
   * @param className the name of the class
   */
  public ClassResource(@Nonnull String containingPackage, @Nonnull String className) {
    super(containingPackage, className);
  }

  /**
   * Provides the name of the class represented by {@code this} resource.
   */
  @Nonnull
  public String getClassName() {
    return resourceName;
  }

  /**
   * Provides the package that contains the class represented by {@code this} resource.
   */
  @Nonnull
  public String getPackage() {
    return resourcePath;
  }
}
