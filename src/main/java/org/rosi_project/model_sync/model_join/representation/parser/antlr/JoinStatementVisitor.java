package org.rosi_project.model_sync.model_join.representation.parser.antlr;

import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.core.OCLConstraint;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression.JoinType;
import org.rosi_project.model_sync.model_join.representation.grammar.NaturalJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.OuterJoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.ThetaJoinExpression;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParsingException;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinBaseVisitor;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.JoinContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.NaturaljoinContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.OuterjoinContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.ThetajoinContext;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.NaturalJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.OuterJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.ThetaJoinBuilder;

/**
 * The {@code JoinStatementVisitor} creates {@link JoinExpression} instances for a parsed {@code
 * ModelJoin} file.
 *
 * @author Rico Bergmann
 */
public class JoinStatementVisitor extends ModelJoinBaseVisitor<JoinExpression> {

  private final KeepStatementVisitor keepStatementVisitor;

  /**
   * Default constructor. Nothing special about it.
   */
  JoinStatementVisitor() {
    this.keepStatementVisitor = new KeepStatementVisitor();
  }

  @Override
  public JoinExpression visitJoin(JoinContext ctx) {
    JoinType joinType = determineJoinType(ctx);
    return invokeJoinBuilderAccordingTo(joinType, ctx);
  }

  /**
   * Checks, which kind of join is specified in a specific {@code JoinContext}.
   */
  private JoinType determineJoinType(@Nonnull JoinContext ctx) {
    if (ctx.thetajoin() != null) {
      return JoinType.THETA;
    } else if (ctx.naturaljoin() != null) {
      return JoinType.NATURAL;
    } else if (ctx.outerjoin() != null) {
      return JoinType.OUTER;
    } else {
      throw new ModelJoinParsingException("Unkown join type in join " + ctx);
    }
  }

  /**
   * Generates a {@link JoinExpression} according to the {@link JoinType} of the given {@code
   * JoinContext}.
   */
  private JoinExpression invokeJoinBuilderAccordingTo(@Nonnull JoinType type,
      @Nonnull JoinContext ctx) {
    switch (type) {
      case THETA:
        return generateThetaJoin(ctx);
      case NATURAL:
        return generateNaturalJoin(ctx);
      case OUTER:
        return generateOuterJoin(ctx);
      default:
        throw new AssertionError(type);
    }
  }

  /**
   * Creates the {@link ThetaJoinExpression} instance contained in a {@code JoinContext}. This
   * method assumes that the provided {@code ctx} indeed contains a theta join.
   */
  private ThetaJoinExpression generateThetaJoin(@Nonnull JoinContext ctx) {
    ThetajoinContext thetajoinContext = ctx.thetajoin();

    ClassResource left = ClassResource.fromQualifiedName(thetajoinContext.classres(0).getText());
    ClassResource right = ClassResource.fromQualifiedName(thetajoinContext.classres(1).getText());

    ThetaJoinBuilder joinBuilder = (ThetaJoinBuilder)JoinFactory.createNew()
        .theta()
        .where(OCLConstraint.of(thetajoinContext.oclcond().getText()))
        .join(left)
        .with(right)
        .as(ClassResource.fromQualifiedName(ctx.classres().getText()));

    ctx.keepaggregatesexpr().stream()
        .map(keepStatementVisitor::visitKeepaggregatesexpr)
        .forEach(joinBuilder::keep);
    ctx.keepattributesexpr().stream()
        .map(keepStatementVisitor::visitKeepattributesexpr)
        .forEach(joinBuilder::keep);
    ctx.keepcalculatedexpr().stream()
        .map(keepStatementVisitor::visitKeepcalculatedexpr)
        .forEach(joinBuilder::keep);
    ctx.keepexpr().stream()
        .map(keepStatementVisitor::visitKeepexpr)
        .forEach(joinBuilder::keep);

    return joinBuilder.done();
  }

  /**
   * Creates the {@link NaturalJoinExpression} instance contained in a {@code JoinContext}. This
   * method assumes that the provided {@code ctx} indeed contains a natural join.
   */
  private NaturalJoinExpression generateNaturalJoin(@Nonnull JoinContext ctx) {
    NaturaljoinContext naturalJoinContext = ctx.naturaljoin();

    ClassResource left = ClassResource.fromQualifiedName(naturalJoinContext.classres(0).getText());
    ClassResource right = ClassResource.fromQualifiedName(naturalJoinContext.classres(1).getText());

    NaturalJoinBuilder joinBuilder = (NaturalJoinBuilder)JoinFactory.createNew()
        .natural()
        .join(left)
        .with(right)
        .as(ClassResource.fromQualifiedName(ctx.classres().getText()));

    ctx.keepaggregatesexpr().stream()
        .map(keepStatementVisitor::visitKeepaggregatesexpr)
        .forEach(joinBuilder::keep);
    ctx.keepattributesexpr().stream()
        .map(keepStatementVisitor::visitKeepattributesexpr)
        .forEach(joinBuilder::keep);
    ctx.keepcalculatedexpr().stream()
        .map(keepStatementVisitor::visitKeepcalculatedexpr)
        .forEach(joinBuilder::keep);
    ctx.keepexpr().stream()
        .map(keepStatementVisitor::visitKeepexpr)
        .forEach(joinBuilder::keep);

    return joinBuilder.done();
  }

  /**
   * Creates the {@link OuterJoinExpression} instance contained in a {@code JoinContext}. This
   * method assumes that the provided {@code ctx} indeed contains an outer join.
   */
  private OuterJoinExpression generateOuterJoin(@Nonnull JoinContext ctx) {
    OuterjoinContext outerJoinContext = ctx.outerjoin();

    ClassResource left = ClassResource.fromQualifiedName(outerJoinContext.classres(0).getText());
    ClassResource right = ClassResource.fromQualifiedName(outerJoinContext.classres(1).getText());

    OuterJoinBuilder joinBuilder = (OuterJoinBuilder)JoinFactory.createNew()
        .outer()
        .join(left)
        .with(right)
        .as(ClassResource.fromQualifiedName(ctx.classres().getText()));

    if (outerJoinContext.leftouterjoin() != null) {
      joinBuilder.leftOuter();
    } else if (outerJoinContext.rightouterjoin() != null) {
      joinBuilder.rightOuter();
    } else {
      throw new ModelJoinParsingException("Expected either 'left' or 'right' 'outer join'");
    }

    ctx.keepaggregatesexpr().stream()
        .map(keepStatementVisitor::visitKeepaggregatesexpr)
        .forEach(joinBuilder::keep);
    ctx.keepattributesexpr().stream()
        .map(keepStatementVisitor::visitKeepattributesexpr)
        .forEach(joinBuilder::keep);
    ctx.keepcalculatedexpr().stream()
        .map(keepStatementVisitor::visitKeepcalculatedexpr)
        .forEach(joinBuilder::keep);
    ctx.keepexpr().stream()
        .map(keepStatementVisitor::visitKeepexpr)
        .forEach(joinBuilder::keep);

    return joinBuilder.done();
  }

}
