package org.rosi_project.model_sync.model_join.representation.parser.legacy;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.core.OCLConstraint;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepExpression;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParsingException;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.NaturalJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.OuterJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.ThetaJoinBuilder;

/**
 * The {@code JoinParser} takes care of reading an individual {@code join statement} and creates the
 * corresponding {@link JoinExpression}.
 * <p>
 * This class is primarily intended to be created and invoked by the {@link DefaultModelJoinParser} only as
 * will modify the state of that as well.
 *
 * @author Rico Bergmann
 */
@Deprecated
class JoinParser {

  private static final String START_OF_JOIN_BLOCK_DELIMITER = "{";
  private static final String JOIN_TYPE_THETA = "theta";
  private static final Pattern JOIN_TYPE_DECLARATION = Pattern
      .compile(".*(?<type>((?<outertype>(right |left )outer)|theta|natural)) join .*");
  private static final Pattern LEFT_SOURCE_DECLARATION = Pattern
      .compile(".*join (?<leftsource>[\\w\\\\.]*) .*");
  private static final Pattern RIGHT_SOURCE_DECLARATION = Pattern
      .compile(".*with (?<rightsource>[\\w\\\\.]*) .*");
  private static final Pattern TARGET_DECLARATION = Pattern
      .compile(".*as (?<target>[\\w\\\\.]*)( .*|\\{)");
  private static final Pattern THETA_PREDICATE_DECLARATION = Pattern
      .compile(".*where (?<predicate>[\\w\\\\.]*)\\{");

  @Nonnull
  private final String startingLine;

  @Nonnull
  private final BufferedReader modelReader;

  private boolean hasReadStartOfJoinBlockCharacter = false;

  /**
   * Full constructor.
   *
   * @param startingLine the line that was last read by the {@link DefaultModelJoinParser} and should
   *     therefore become the starting point for {@code this} parser.
   * @param modelReader the reader that provides access to the following lines in the {@code
   *     ModelJoin} file currently being read.
   */
  JoinParser(@Nonnull String startingLine, @Nonnull BufferedReader modelReader) {
    this.startingLine = startingLine;
    this.modelReader = modelReader;
  }

  /**
   * Creates the {@link JoinExpression} from the current {@code join statement}.
   * <p>
   * If something goes wrong, either an {@code IOException} will be thrown directly or a {@link
   * ModelJoinParsingException} is used.
   */
  @Nonnull
  public JoinExpression run() throws IOException {

    // first, we will determine the Join type

    Matcher joinTypeDeclarationMatcher = JOIN_TYPE_DECLARATION.matcher(startingLine);

    if (!joinTypeDeclarationMatcher.matches()) {
      throw new ModelJoinParsingException(
          "Expected beginning of a Join declaration on '" + startingLine + "'");
    }

    String joinType = joinTypeDeclarationMatcher.group("type");
    String outerJoinType = "";

    if (joinType.equals("outer")) {
      outerJoinType = joinTypeDeclarationMatcher.group("outertype");
    }

    // second, we will try to extract all necessary info from the first line

    String leftSource = null;
    String rightSource = null;
    String target = null;
    String thetaPredicate = null;

    Matcher leftSourceMatcher = LEFT_SOURCE_DECLARATION.matcher(startingLine);
    Matcher rightSourceMatcher = RIGHT_SOURCE_DECLARATION.matcher(startingLine);
    Matcher targetMatcher = TARGET_DECLARATION.matcher(startingLine);
    Matcher thetaPredicateMatcher = THETA_PREDICATE_DECLARATION.matcher(startingLine);

    final int necessaryComponents;
    int foundComponents = 0;

    if (leftSourceMatcher.matches()) {
      leftSource = leftSourceMatcher.group("leftsource");
      foundComponents++;
    }

    if (rightSourceMatcher.matches()) {
      rightSource = rightSourceMatcher.group("rightsource");
      foundComponents++;
    }

    if (targetMatcher.matches()) {
      target = targetMatcher.group("target");
      foundComponents++;
    }

    if (joinType.equals(JOIN_TYPE_THETA)) {
      necessaryComponents = 4;
      if (thetaPredicateMatcher.matches()) {
        thetaPredicate = thetaPredicateMatcher.group("predicate");
        foundComponents++;
      }
    } else {
      necessaryComponents = 3;
    }

    // in case all information is present on the first line, we are done already

    if (startingLine.contains(START_OF_JOIN_BLOCK_DELIMITER)) {
      // TODO could the start character be part of an OCL statement?
      hasReadStartOfJoinBlockCharacter = true;
    }

    // otherwise, we need to read the following lines as well

    String currentLine = "";
    while (!hasReadStartOfJoinBlockCharacter //
        && foundComponents < necessaryComponents //
        && (currentLine = modelReader.readLine()) != null) {

      // again, we try to extract as much as possible of the info we need
      // however, we now also have to check, whether we have already specified a component, as well

      leftSourceMatcher = LEFT_SOURCE_DECLARATION.matcher(currentLine);
      if (leftSource == null && leftSourceMatcher.matches()) {
        leftSource = leftSourceMatcher.group("leftsource");
        foundComponents++;
      }

      rightSourceMatcher = RIGHT_SOURCE_DECLARATION.matcher(currentLine);
      if (rightSource == null && rightSourceMatcher.matches()) {
        rightSource = rightSourceMatcher.group("rightsource");
        foundComponents++;
      }

      targetMatcher = TARGET_DECLARATION.matcher(currentLine);
      if (target == null && targetMatcher.matches()) {
        target = targetMatcher.group("target");
      }

      if (joinType.equals(JOIN_TYPE_THETA)) {
        thetaPredicateMatcher = THETA_PREDICATE_DECLARATION.matcher(currentLine);
        if (thetaPredicate == null && thetaPredicateMatcher.matches()) {
          thetaPredicate = thetaPredicateMatcher.group("predicate");
          foundComponents++;
        }
      }

      if (currentLine.contains(START_OF_JOIN_BLOCK_DELIMITER)) {
        hasReadStartOfJoinBlockCharacter = true;
      }
    }

    // if we found the beginning of the Join body, but did not read all our information, the
    // ModelJoin file is flawed

    if (foundComponents < necessaryComponents) {
      throw new ModelJoinParsingException(
          "ModelJoin grammar violation: found components " + foundComponents + " but expected "
              + necessaryComponents);
    }

    // otherwise we are ready to go. So we skip forward until the Join body is reached.

    while (!hasReadStartOfJoinBlockCharacter && (currentLine = modelReader.readLine()) != null) {
      if (currentLine.contains(START_OF_JOIN_BLOCK_DELIMITER)) {
        hasReadStartOfJoinBlockCharacter = true;
      }
    }

    // if we read everything, but did not find the Join body, the ModelJoin file is flawed again

    if (!hasReadStartOfJoinBlockCharacter) {
      throw new ModelJoinParsingException(
          "Expected start of join block but could not read any further");
    }

    // if not all components are initialized by now, we are working on a flawed ModelJoin file

    if (currentLine == null || leftSource == null || rightSource == null || target == null) {
      throw new ModelJoinParsingException(
          "Not all required information was found but could not read any further");
    }

    // at this point the declaration of the join statement is valid
    // therefore we want the keep expressions it contains. This is the KeepParser's job.

    KeepParser keepParser = new KeepParser(currentLine, modelReader);
    Collection<KeepExpression> keeps = keepParser.run();

    // finally we have to create the correct JoinExpression

    JoinFactory joinFactory = JoinFactory.createNew();
    switch (joinType) {
      case "outer":
        OuterJoinBuilder outerJoinBuilder = joinFactory.outer();
        if (outerJoinType.equals("left")) {
          outerJoinBuilder.leftOuter();
        } else if (outerJoinType.equals("right")) {
          outerJoinBuilder.rightOuter();
        } else {
          throw new ModelJoinParsingException("Unknown direction for outer join: " + outerJoinType);
        }
        outerJoinBuilder //
            .join(ClassResource.fromQualifiedName(leftSource)) //
            .with(ClassResource.fromQualifiedName(rightSource)) //
            .as(ClassResource.fromQualifiedName(target));
        keeps.forEach(outerJoinBuilder::keep);
        return outerJoinBuilder.done();
      case "theta":
        ThetaJoinBuilder thetaJoinBuilder = (ThetaJoinBuilder)joinFactory
            .theta()
            .where(OCLConstraint.of(thetaPredicate))
            .join(ClassResource.fromQualifiedName(leftSource))
            .with(ClassResource.fromQualifiedName(rightSource)) 
            .as(ClassResource.fromQualifiedName(target));
        keeps.forEach(thetaJoinBuilder::keep);
        return thetaJoinBuilder.done();
      case "natural":
        NaturalJoinBuilder naturalJoinBuilder = (NaturalJoinBuilder)joinFactory //
            .natural() //
            .join(ClassResource.fromQualifiedName(leftSource)) //
            .with(ClassResource.fromQualifiedName(rightSource)) //
            .as(ClassResource.fromQualifiedName(target));
        keeps.forEach(naturalJoinBuilder::keep);
        return naturalJoinBuilder.done();
      default:
        throw new ModelJoinParsingException("Unknown join type: " + joinType);
    }
  }
}
