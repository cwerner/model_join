package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;

/**
 * A {@code CompoundKeepExpression} is a special kind of {@code KeepExpression}
 * which in turn may contain a number of other {@code KeepExpression}s.
 *
 * @author Rico Bergmann
 */
public abstract class CompoundKeepExpression extends KeepExpression {

	@Nonnull
	protected List<KeepExpression> keeps;

	/**
	 * Provides all {@code KeepExpression keep expressions} that should be used
	 * to build the Join for the instances of the subclass, superclass or
	 * referenced instances.
	 */
	@Nonnull
	public List<KeepExpression> getKeeps() {
		return new ArrayList<>(keeps);
	}

	public boolean addKeepExpression(KeepExpression keepExpression) {
		return keeps.add(keepExpression);
	}
}
