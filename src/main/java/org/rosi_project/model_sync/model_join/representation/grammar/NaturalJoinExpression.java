package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.List;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;

/**
 * The {@code natural join} combines model classes based on attributes of the
 * same name and type.
 * <p>
 * As an example, consider the following two classes:
 * 
 * <pre>
 * {
 * 	&#64;code
 * 	class Person {
 * 		String firstname;
 * 		String lastname;
 * 		Date dateOfBirth;
 * 		Char gender;
 * 		// ... other code omitted
 * 	}
 *
 * 	class Employee {
 * 		int id;
 * 		String firstname;
 * 		String lastname;
 * 		Date dateOfBirth;
 * 		int salary;
 * 		// ... other code omitted
 * 	}
 * }
 * </pre>
 * <p>
 * Performing a natural join on {@code Person} and {@code Employee} will combine
 * instances only if they share the same first name, last name and date of
 * birth. All other fields will not be considered during the comparison.
 * 
 * <pre>
 * ┌───────────────────┐             ┌───────────────────┐
 * │ Person            │             │ Employee          │
 * ├───────────────────┤             ├───────────────────┤
 * │firstname: String  │ ┄┄┄┄┄┄┄╮    │id: int            │
 * │lastname: String   │ ┄┄┄┄┄╮ ╰┄┄┄ │firstname: String  │
 * │dateOfBirth: Date  │ ┄┄┄╮ ╰┄┄┄┄┄ │lastname: String   │
 * │gender: Char       │    ╰┄┄┄┄┄┄┄ │dateOfBirth: Date  │
 * ├───────────────────┤             │salary: int        │
 * │ ...               │             ├───────────────────┤
 * └───────────────────┘             │ ...               │
 *                                   └───────────────────┘
 * </pre>
 * <p>
 * The resulting class will by default only contain the attributes that where
 * used in comparison (first name, last name and date of birth in the example).
 *
 * @author Rico Bergmann
 */
public class NaturalJoinExpression extends JoinExpression {

	/*
	 * TODO: discuss should the natural join extend theta join instead (from a
	 * theoretical view more appropriate. However this would also mean that the
	 * natural join has to derive the join condition as well. This in turn would
	 * also meant that class resources would have to know which attributes are
	 * defined on them.
	 */

	/**
	 * Full constructor.
	 *
	 * @param left
	 *            the left class to use in the join
	 * @param right
	 *            the right class to use in the join
	 * @param target
	 *            the name of the resulting view
	 * @param keeps
	 *            the keep statements in the join
	 */
	public NaturalJoinExpression(@Nonnull ClassResource left, @Nonnull ClassResource right,
			@Nonnull ClassResource target, @Nonnull List<KeepExpression> keeps) {
		super(left, right, target, keeps);
	}

	@Nonnull
	@Override
	public JoinType getType() {
		return JoinType.NATURAL;
	}

	@Nonnull
	@Override
	String getJoinTypeAsString() {
		return "natural join";
	}
}
