package org.rosi_project.model_sync.model_join.representation.core;

import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * An {@code OCLStatement} is a simple expression written in the {@code Object Constraint Language}.
 * <p>
 * This class simply wraps an arbitrary OCL expression. No further checks or the like are performed.
 *
 * @author Rico Bergmann
 * @see <a href="https://www.omg.org/spec/OCL/">OMG OCL specification</a>
 */
public class OCLStatement {

  /**
   * Creates a new statement with the given {@code content}.
   */
  @Nonnull
  public static OCLStatement of(@Nonnull String content) {
    return new OCLStatement(content);
  }

  @Nonnull
  protected final String content;

  /**
   * Full constructor.
   *
   * @param content the actual expression
   */
  public OCLStatement(@Nonnull String content) {
    this.content = content;
  }

  /**
   * Provides the actual statement as a {@code String}.
   */
  public String get() {
    return content;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OCLStatement that = (OCLStatement) o;
    return content.equals(that.content);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content);
  }

  @Override
  public String toString() {
    return content;
  }
}
