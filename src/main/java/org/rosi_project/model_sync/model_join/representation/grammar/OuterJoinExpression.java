package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.List;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;

/**
 * The {@code outer join} is a special kind of {@code natural join}: depending
 * on its specific type, all elements of one source will definitely be included
 * - potentially being joined with matching instances of the other source.
 * <p>
 * Matches will be determined the same way as with a normal
 * {@code natural join}. However, depending on the kind of join, the result will
 * include all instances of the {@code left} or {@code right} source model, no
 * matter whether they have a matching instance in the other model or not. In
 * case that some instance has no corresponding instance in the other model, all
 * fields that belong to the other model will be set to {@code null}.
 * <p>
 * Consider the following example (derived from the model shown in the
 * {@link NaturalJoinExpression} documentation)
 * 
 * <pre>
 * Person:
 * ┌───────────┬──────────┬─────────────┬────────┐
 * │ firstname │ lastname │ dateOfBirth │ gender │
 * ├───────────┼──────────┼─────────────┼────────┤
 * │ Jane      │ Doe      │ 1999-01-01  │   f    │
 * │ John      │ Doe      │ 1998-02-03  │   m    │
 * │ Maria     │ Miller   │ 1990-12-31  │   f    │
 * └───────────┴──────────┴─────────────┴────────┘
 *
 * Employee:
 * ┌────┬───────────┬──────────┬─────────────┬────────┐
 * │ id │ firstname │ lastname │ dateOfBirth │ salary │
 * ├────┼───────────┼──────────┼─────────────┼────────┤
 * │ 1  │ Jane      │ Doe      │ 1999-01-01  │ 20000  │
 * │ 2  │ John      │ Doe      │ 1997-03-04  │ 15000  │
 * │ 3  │ Maria     │ Miller   │ 1990-12-31  │ 17500  │
 * └────┴───────────┴──────────┴─────────────┴────────┘
 * </pre>
 * 
 * Performing an left outer join as described in the
 * {@link NaturalJoinExpression natural join} documentation will lead to the
 * following result (again written in relational style):
 * 
 * <pre>
 * ┌───────────┬──────────┬─────────────┬────────┬──────┬────────┐
 * │ firstname │ lastname │ dateOfBirth │ gender │ id   │ salary │
 * ├───────────┼──────────┼─────────────┼────────┼──────┼────────┤
 * │ Jane      │ Doe      │ 1999-01-01  │   f    │ 1    │ 20000  │
 * │ John      │ Doe      │ 1998-02-03  │   m    │ NULL │ NULL   │
 * │ Maria     │ Miller   │ 1990-12-31  │   f    │ 3    │ 17500  │
 * └───────────┴──────────┴─────────────┴────────┴──────┴────────┘
 * </pre>
 *
 * @author Rico Bergmann
 */
public class OuterJoinExpression extends NaturalJoinExpression {

	/**
	 * The {@code JoinDirection} denotes which side of the source models should
	 * keep all of its instances - no matter whether they have a matching
	 * instances in the other model or not.
	 */
	public enum JoinDirection {
		LEFT, RIGHT
	}

	@Nonnull
	private final JoinDirection direction;

	/**
	 * Full constructor.
	 *
	 * @param left
	 *            the left class to use in the join
	 * @param right
	 *            the right class to use in the join
	 * @param target
	 *            the name of the resulting view
	 * @param direction
	 *            the source model which should definitely be kept
	 * @param keeps
	 *            the keep statements in the join
	 */
	public OuterJoinExpression(@Nonnull ClassResource left, @Nonnull ClassResource right, @Nonnull ClassResource target,
			@Nonnull JoinDirection direction, @Nonnull List<KeepExpression> keeps) {
		super(left, right, target, keeps);
		this.direction = direction;
	}

	/**
	 * Provides the position of the source model on which the join should be
	 * based.
	 */
	@Nonnull
	public JoinDirection getDirection() {
		return direction;
	}

	/**
	 * Provides the source model on which the join should be based.
	 */
	@Override
	public ClassResource getBaseModel() {
		switch (direction) {
		case LEFT:
			return left;
		case RIGHT:
			return right;
		default:
			throw new AssertionError(direction);
		}
	}

	@Override
	public ClassResource getOtherModel() {
		switch (direction) {
		case LEFT:
			return right;
		case RIGHT:
			return left;
		default:
			throw new AssertionError(direction);
		}
	}

	@Nonnull
	@Override
	public JoinType getType() {
		return JoinType.OUTER;
	}

	@Nonnull
	@Override
	String getJoinTypeAsString() {
		String directionString;
		switch (this.direction) {
		case LEFT:
			directionString = "left";
			break;
		case RIGHT:
			directionString = "right";
			break;
		default:
			throw new AssertionError(this.direction);
		}
		return directionString + " outer join";
	}
}
