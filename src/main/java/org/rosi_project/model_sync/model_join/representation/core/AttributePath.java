package org.rosi_project.model_sync.model_join.representation.core;

import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * An {@code AttributePath} represents a single attribute of some model class.
 * <p>
 * As <em>ModelJoin</em> does not care about types at a conceptual level, no type information is
 * stored.
 *
 * @author Rico Bergmann
 */
public class AttributePath {

  private static final String CLASS_ATTRIBUTE_DELIMITER = ".";

  /**
   * Generates a new {@code path} given the owning class as well as the attribute's name.
   */
  @Nonnull
  public static AttributePath from(
      @Nonnull ClassResource containingClass,
      @Nonnull String attributeName) {
    return new AttributePath(containingClass, attributeName);
  }

  /**
   * Generates a new {@code path} given the owning class as well as the attribute.
   */
  @Nonnull
  public static AttributePath from(
      @Nonnull ClassResource containingClass,
      @Nonnull RelativeAttribute attribute) {
    return new AttributePath(containingClass, attribute);
  }

  public static AttributePath from(String containingClass, String attributeName) {
    return new AttributePath(containingClass, attributeName);
  }

  /**
   * Generates a new {@code AttributePath} by splitting up the {@code qualifiedPath} into the
   * {@link ClassResource} and attribute portion. Therefore the {@code qualifiedPath} is expected
   * to adhere to the following scheme: {@code [package].class.attribute}.
   */
  @Nonnull
  public static AttributePath fromQualifiedPath(@Nonnull String qualifiedPath) {
    final int splitPos = qualifiedPath.lastIndexOf(CLASS_ATTRIBUTE_DELIMITER);

    if (splitPos < 0) {
      throw new IllegalArgumentException("Missing class portion on '" + qualifiedPath + "'");
    }

    String classResourcePortion = qualifiedPath.substring(0, splitPos);
    String attributePortion = qualifiedPath.substring(splitPos + 1);
    return new AttributePath(ClassResource.fromQualifiedName(classResourcePortion), attributePortion);
  }

  @Nonnull
  private final ClassResource containingClass;

  @Nonnull
  private final RelativeAttribute attribute;

  public AttributePath(String containingClass, String attributeName) {
    this.containingClass = ClassResource.fromQualifiedName(containingClass);
    this.attribute = RelativeAttribute.of(attributeName);
  }

  /**
   * Full constructor, parsing the attribute's name into a valid {@link RelativeAttribute}.
   *
   * @param containingClass the class which owns the attribute
   * @param attributeName the attribute's name
   */
  public AttributePath(@Nonnull ClassResource containingClass, @Nonnull String attributeName) {
    this.containingClass = containingClass;
    this.attribute = RelativeAttribute.of(attributeName);
  }

  /**
   * Full constructor.
   *
   * @param containingClass the class which owns the attribute
   * @param attribute the attribute's name
   */
  public AttributePath(@Nonnull ClassResource containingClass, @Nonnull RelativeAttribute attribute) {
    this.containingClass = containingClass;
    this.attribute = attribute;
  }

  /**
   * Creates a new {@code AttributePath} by copying another path.
   */
  protected AttributePath(@Nonnull AttributePath other) {
    this.containingClass = other.getContainingClass();
    this.attribute = other.getAttribute();
  }

  /**
   * Provides the class which owns {@code this} attribute.
   */
  @Nonnull
  public ClassResource getContainingClass() {
    return containingClass;
  }

  /**
   * Provides {@code this} attribute as a {@link RelativeAttribute}.
   */
  @Nonnull
  public RelativeAttribute getAttribute() {
    return attribute;
  }

  /**
   * Provides the name of {@code this} attribute.
   */
  @Nonnull
  public String getAttributeName() {
    return attribute.getAttributeName();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AttributePath)) {
      return false;
    }
    AttributePath that = (AttributePath) o;
    return containingClass.equals(that.containingClass) &&
        attribute.equals(that.attribute);
  }

  @Override
  public int hashCode() {
    return Objects.hash(containingClass, attribute);
  }

  @Override
  public String toString() {
    return containingClass.toString() + "." + attribute;
  }
}
