package org.rosi_project.model_sync.model_join.representation.parser.antlr;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.core.OCLStatement;
import org.rosi_project.model_sync.model_join.representation.core.RelativeAttribute;
import org.rosi_project.model_sync.model_join.representation.core.TypedAttributePath;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAggregateExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAggregateExpression.AggregationType;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAttributesExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepCalculatedExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepReferenceExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSubTypeExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSuperTypeExpression;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParsingException;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinBaseVisitor;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.AttrresContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepaggregatesexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepattributesexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepcalculatedexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepincomingexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepoutgoingexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepsubtypeexprContext;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.KeepsupertypeexprContext;

/**
 * The {@code KeepStatementVisitor} creates {@link KeepExpression} instances from a parsed {@code
 * ModelJoin} file.
 *
 * @author Rico Bergmann
 */
public class KeepStatementVisitor extends ModelJoinBaseVisitor<KeepExpression> {

  /**
   * A simple enumeration of the possible keep statements for compound statements as defined in the
   * {@code ModelJoin} ANTLR grammar.
   */
  private enum KeepExprType {

    /**
     * The {@code KEEP_TYPE_EXPR} subsumes both {@code keep subtype} as well as {@code keep
     * supertype} expressions.
     */
    KEEP_TYPE_EXPR,

    /**
     * The {@code KEEP_INCOMING_EXPR} corresponds to a {@code keep incoming reference} statement.
     */
    KEEP_INCOMING_EXPR,

    /**
     * The {@code KEEP_OUTGOING_EXPR} corresponds to a {@code keep outgoing reference} statement.
     */
    KEEP_OUTGOING_EXPR

  }

  @Override
  public KeepExpression visitKeepattributesexpr(KeepattributesexprContext ctx) {
    return new KeepAttributesExpression(
        ctx.attrres().stream()
            .map(AttrresContext::getText)
            .map(AttributePath::fromQualifiedPath)
            .collect(ArrayList::new, ArrayList::add, ArrayList::addAll)
    );

  }

  @Override
  public KeepExpression visitKeepaggregatesexpr(KeepaggregatesexprContext ctx) {
    String rawAggregationType = ctx.aggrtype().getText();
    AggregationType aggregationType = AggregationType.valueOf(rawAggregationType.toUpperCase());

    return new KeepAggregateExpression(
        aggregationType /* SUM / MAX / MIN etc */,
        RelativeAttribute.of(ctx.relattr().getText()) /* the attribute in the target relation  */,
        AttributePath.fromQualifiedPath(ctx.classres().getText()) /* the source attribute */,
        AttributePath.fromQualifiedPath(ctx.attrres().getText()) /* name in the new ModelJoin */
    );
  }

  @Override
  public KeepExpression visitKeepcalculatedexpr(KeepcalculatedexprContext ctx) {
    OCLStatement oclExpr = OCLStatement.of(ctx.oclcond().getText());

    return KeepCalculatedExpression //
        .keepCalculatedAttribute(oclExpr) //
        .as(TypedAttributePath.fromQualifiedTypedPath(ctx.typedattrres().getText())
        );
  }

  @Override
  public KeepExpression visitKeepexpr(KeepexprContext ctx) {
    KeepExprType keepExprType = determineExpressionTypeFor(ctx);

    switch (keepExprType) {
      case KEEP_TYPE_EXPR:
        return buildKeepTypeExpressionFor(ctx);
      case KEEP_INCOMING_EXPR:
        return buildKeepIncomingReferenceExpressionFor(ctx);
      case KEEP_OUTGOING_EXPR:
        return buildKeepOutgoingReferenceExpressionFor(ctx);
      default:
        throw new AssertionError(keepExprType);
    }
  }

  /**
   * Checks, which kind of expression is specified in a specific {@code KeepexprContext}.
   */
  private KeepExprType determineExpressionTypeFor(KeepexprContext ctx) {

    /*
     * From the ANTLR grammar we know that exactly one of the IF cases has to hold
     */
    if (ctx.keeptypeexpr() != null) {
      return KeepExprType.KEEP_TYPE_EXPR;
    } else if (ctx.keepincomingexpr() != null) {
      return KeepExprType.KEEP_INCOMING_EXPR;
    } else if (ctx.keepoutgoingexpr() != null) {
      return KeepExprType.KEEP_OUTGOING_EXPR;
    } else {
      throw new ModelJoinParsingException(
          "Expected either 'keep reference' or 'keep type'  expression: " + ctx);
    }
  }

  /**
   * Creates the appropriate {@link KeepExpression} for a {@code KeepexprContext}.
   */
  private KeepExpression buildKeepTypeExpressionFor(KeepexprContext ctx) {
    if (ctx.keeptypeexpr().keepsubtypeexpr() != null) {
      return buildKeepSubtypeExpressionFor(ctx);
    } else if (ctx.keeptypeexpr().keepsupertypeexpr() != null) {
      return buildKeepSupertypeExpressionFor(ctx);
    } else {
      throw new ModelJoinParsingException(
          "Expected either 'keep supertype' or 'keep subtype'  expression: " + ctx);
    }
  }

  /**
   * Creates the {@link KeepReferenceExpression} instance contained in a {@code KeepexprContext}.
   * This method assumes that the provided {@code ctx} indeed contains a keep incoming reference
   * statement.
   */
  private KeepReferenceExpression buildKeepIncomingReferenceExpressionFor(KeepexprContext ctx) {
    KeepincomingexprContext incomingCtx = ctx.keepincomingexpr();

    AttributePath attributeToKeep = AttributePath
        .fromQualifiedPath(incomingCtx.attrres().getText());

    List<KeepExpression> nestedExpressions = ctx.keepattributesexpr().stream()
        .map(this::visitKeepattributesexpr)
        .collect(Collectors.toList());

    nestedExpressions.addAll(ctx.keepaggregatesexpr().stream()
        .map(this::visitKeepaggregatesexpr)
        .collect(Collectors.toList())
    );

    nestedExpressions.addAll(ctx.keepexpr().stream()
        .map(this::visitKeepexpr)
        .collect(Collectors.toList())
    );

    KeepReferenceExpression.KeepReferenceBuilder expressionBuilder = KeepReferenceExpression.keep()
        .incoming(attributeToKeep);

    ClassResource targetClass;
    if (incomingCtx.classres() != null) {
      targetClass = ClassResource.fromQualifiedName(incomingCtx.classres().getText());
    } else {
      targetClass = attributeToKeep.getContainingClass();
    }

    expressionBuilder.as(targetClass);
    nestedExpressions.forEach(expressionBuilder::keep);

    return expressionBuilder.buildExpression();
  }

  /**
   * Creates the {@link KeepReferenceExpression} instance contained in a {@code KeepexprContext}.
   * This method assumes that the provided {@code ctx} indeed contains a keep outgoing reference
   * statement.
   */
  private KeepReferenceExpression buildKeepOutgoingReferenceExpressionFor(KeepexprContext ctx) {
    KeepoutgoingexprContext outgoingCtx = ctx.keepoutgoingexpr();

    AttributePath attributeToKeep = AttributePath
        .fromQualifiedPath(outgoingCtx.attrres().getText());

    List<KeepExpression> nestedExpressions = ctx.keepattributesexpr().stream()
        .map(this::visitKeepattributesexpr)
        .collect(Collectors.toList());

    nestedExpressions.addAll(ctx.keepaggregatesexpr().stream()
        .map(this::visitKeepaggregatesexpr)
        .collect(Collectors.toList())
    );

    nestedExpressions.addAll(ctx.keepexpr().stream()
        .map(this::visitKeepexpr)
        .collect(Collectors.toList())
    );

    KeepReferenceExpression.KeepReferenceBuilder expressionBuilder = KeepReferenceExpression.keep()
        .outgoing(attributeToKeep);

    ClassResource targetClass;
    if (outgoingCtx.classres() != null) {
      targetClass = ClassResource.fromQualifiedName(outgoingCtx.classres().getText());
    } else {
      targetClass = attributeToKeep.getContainingClass();
    }

    expressionBuilder.as(targetClass);
    nestedExpressions.forEach(expressionBuilder::keep);

    return expressionBuilder.buildExpression();
  }

  /**
   * Creates the {@link KeepSubTypeExpression} instance contained in a {@code KeepexprContext}. This
   * method assumes that the provided {@code ctx} indeed contains a keep subtype statement.
   */
  private KeepSubTypeExpression buildKeepSubtypeExpressionFor(KeepexprContext ctx) {
    KeepsubtypeexprContext subtypeCtx = ctx.keeptypeexpr().keepsubtypeexpr();

    ClassResource typeToKeep = ClassResource.fromQualifiedName(subtypeCtx.classres(0).getText());

    List<KeepExpression> nestedExpressions = ctx.keepattributesexpr().stream()
        .map(this::visitKeepattributesexpr)
        .collect(Collectors.toList());

    nestedExpressions.addAll(ctx.keepaggregatesexpr().stream()
        .map(this::visitKeepaggregatesexpr)
        .collect(Collectors.toList())
    );

    nestedExpressions.addAll(ctx.keepexpr().stream()
        .map(this::visitKeepexpr)
        .collect(Collectors.toList())
    );

    KeepSubTypeExpression.KeepSubTypeBuilder expressionBuilder = KeepSubTypeExpression.keep()
        .subtype(typeToKeep);

    ClassResource targetClass;
    if (subtypeCtx.classres() != null) {
      targetClass = ClassResource.fromQualifiedName(subtypeCtx.classres(1).getText());
    } else {
      targetClass = typeToKeep;
    }

    expressionBuilder.as(targetClass);
    nestedExpressions.forEach(expressionBuilder::keep);

    return expressionBuilder.buildExpression();
  }

  /**
   * Creates the {@link KeepSuperTypeExpression} instance contained in a {@code KeepexprContext}.
   * This method assumes that the provided {@code ctx} indeed contains a keep supertype statement.
   */
  private KeepSuperTypeExpression buildKeepSupertypeExpressionFor(KeepexprContext ctx) {
    KeepsupertypeexprContext supertypeCtx = ctx.keeptypeexpr().keepsupertypeexpr();

    ClassResource typeToKeep = ClassResource.fromQualifiedName(supertypeCtx.classres(0).getText());

    List<KeepExpression> nestedExpressions = ctx.keepattributesexpr().stream()
        .map(this::visitKeepattributesexpr)
        .collect(Collectors.toList());

    nestedExpressions.addAll(ctx.keepaggregatesexpr().stream()
        .map(this::visitKeepaggregatesexpr)
        .collect(Collectors.toList())
    );

    nestedExpressions.addAll(ctx.keepexpr().stream()
        .map(this::visitKeepexpr)
        .collect(Collectors.toList())
    );

    KeepSuperTypeExpression.KeepSuperTypeBuilder expressionBuilder = KeepSuperTypeExpression.keep()
        .supertype(typeToKeep);

    ClassResource targetClass;
    if (supertypeCtx.classres() != null) {
      targetClass = ClassResource.fromQualifiedName(supertypeCtx.classres(1).getText());
    } else {
      targetClass = typeToKeep;
    }

    expressionBuilder.as(targetClass);
    nestedExpressions.forEach(expressionBuilder::keep);

    return expressionBuilder.buildExpression();
  }

}
