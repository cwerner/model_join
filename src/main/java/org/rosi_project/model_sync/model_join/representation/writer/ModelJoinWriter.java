package org.rosi_project.model_sync.model_join.representation.writer;

import java.io.File;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;

/**
 * The {@code ModelJoinWriter} acts as an entry point to write and or transform {@link
 * ModelJoinExpression model join descriptions} to various representations.
 *
 * @author Rico Bergmann
 */
public class ModelJoinWriter {

  private static final StringBasedModelJoinWriter stringWriter = StringBasedModelJoinWriter
      .createDefault();

  /**
   * Converts a whole model join description to a single {@code String}.
   * <p>
   * This {@code String} is primarily intended to be human-readable and does not focus in being easy
   * to reuse for further purposes.
   */
  public static String writeToString(@Nonnull ModelJoinExpression modelJoin) {
    return stringWriter.write(modelJoin);
  }

  /**
   * Outputs a whole model join description to a single file.
   * <p>
   * The resulting file is intended to be both human-readable as well as to be parsed by further
   * applications.
   *
   * @return whether the expression was written successfully.
   */
  public static boolean writeToFile(
      @Nonnull File outputFile,
      @Nonnull ModelJoinExpression modelJoin) {
    return new FileBasedModelJoinWriter(outputFile).write(modelJoin);
  }

}
