package org.rosi_project.model_sync.model_join.representation.parser.antlr;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParser;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParsingException;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinLexer;

/**
 * The {@code AntlrBackedModelJoinParser} reads a {@code ModelJoin} file according to a compiled
 * {@code ANTLR} grammar and generates the matching {@link ModelJoinExpression} for the file.
 *
 * @author Rico Bergmann
 * @see <a href="https://www.antlr.org/">ANTLR project</a>
 */
public class AntlrBackedModelJoinParser implements ModelJoinParser {

  @Nonnull
  @Override
  public Optional<ModelJoinExpression> read(@Nonnull File modelFile) {
    try {
      return Optional.of(invokeAntlr(modelFile));
    } catch (ModelJoinParsingException e) {
      return Optional.empty();
    }
  }

  @Nonnull
  @Override
  public ModelJoinExpression readOrThrow(@Nonnull File modelFile) {
    return invokeAntlr(modelFile);
  }

  /**
   * Executes the whole ANTLR workflow.
   *
   * @throws ModelJoinParsingException if the workflow fails. Details on the error will be added
   *     to the exception.
   */
  protected ModelJoinExpression invokeAntlr(@Nonnull File modelFile) {
    CharStream modelStream = consumeCompleteModelFileContent(modelFile);
    ModelJoinLexer lexer = initializeLexerBasedOn(modelStream);
    TokenStream tokenStream = tokenize(lexer);

    org.rosi_project.model_sync.model_join.representation.parser.antlr.generated. //
        ModelJoinParser modelJoinParser = initializeParserBasedOn(tokenStream);

    org.rosi_project.model_sync.model_join.representation.parser.antlr.generated. //
        ModelJoinParser.ModeljoinContext modelJoinContext = modelJoinParser.modeljoin();

    ModelJoinVisitor modelJoinVisitor = new ModelJoinVisitor();

    return modelJoinVisitor.visitModeljoin(modelJoinContext);
  }

  /**
   * Converts the content of a {@code ModelJoin} file into an ANTLR input stream.
   *
   * @throws ModelJoinParsingException if the file may not be read
   */
  @Nonnull
  protected CharStream consumeCompleteModelFileContent(@Nonnull File modelFile) {
    try {
      return CharStreams.fromPath(modelFile.toPath());
    } catch (IOException e) {
      throw new ModelJoinParsingException(e);
    }
  }

  /**
   * Sets up a lexer for the given {@code ModelJoin} file.
   */
  @Nonnull
  protected ModelJoinLexer initializeLexerBasedOn(@Nonnull CharStream modelFile) {
    return new ModelJoinLexer(modelFile);
  }

  /**
   * Executes the lexing step.
   */
  @Nonnull
  protected TokenStream tokenize(@Nonnull ModelJoinLexer lexer) {
    return new CommonTokenStream(lexer);
  }

  /**
   * Sets up the parser.
   */
  @Nonnull
  protected org.rosi_project.model_sync.model_join.representation.parser.antlr.generated. //
      ModelJoinParser initializeParserBasedOn(@Nonnull TokenStream tokens) {
    return new org.rosi_project.model_sync.model_join.representation.parser.antlr.generated. //
        ModelJoinParser(tokens);
  }

}

