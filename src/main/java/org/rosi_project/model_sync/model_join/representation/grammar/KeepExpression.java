package org.rosi_project.model_sync.model_join.representation.grammar;

import javax.annotation.Nonnull;

/**
 * A {@code KeepExpression} configures the resulting model of a join.
 * <p>
 * This mostly means retaining some attributes or calculating their value but may also include
 * maintaining references to other classes.
 *
 * @author Rico Bergmann
 */
public abstract class KeepExpression {

  /**
   * The {@code visitor} enables the execution of arbitrary algorithms on {@code KeepExpression}s
   * without taking care of the actual traversal of the expression tree.
   */
  public interface KeepExpressionVisitor {

    /**
     * Runs the algorithm as appropriate for {@link KeepAggregateExpression}s.
     */
    void visit(@Nonnull KeepAggregateExpression expression);

    /**
     * Runs the algorithm as appropriate for {@link KeepAttributesExpression}s.
     */
    void visit(@Nonnull KeepAttributesExpression expression);

    /**
     * Runs the algorithm as appropriate for {@link KeepCalculatedExpression}s.
     */
    void visit(@Nonnull KeepCalculatedExpression expression);

    /**
     * Runs the algorithm as appropriate for {@link KeepReferenceExpression}s.
     */
    void visit(@Nonnull KeepReferenceExpression expression);

    /**
     * Runs the algorithm as appropriate for {@link KeepSubTypeExpression}s.
     */
    void visit(@Nonnull KeepSubTypeExpression expression);

    /**
     * Runs the algorithm as appropriate for {@link KeepSuperTypeExpression}s.
     */
    void visit(@Nonnull KeepSuperTypeExpression expression);

  }

  // TODO `as` statements may be omitted !?!? ... (╯°□°）╯︵ ┻━┻

  /**
   * Applies a {@code visitor} to {@code this} expression structure.
   */
  public abstract void accept(@Nonnull KeepExpressionVisitor visitor);

}
