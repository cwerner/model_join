package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.util.Assert;

// TODO add doc

/**
 * A {@code KeepSuperTypeExpression} instructs the {@code ModelJoin} runtime to instantiate the a
 * supertype of some class in a join along an instance of that class. The additional instance will
 * be built according to the {@code KeepExpression keep statements} that it is constructed of.
 *
 * @author Rico Bergmann
 */
public class KeepSuperTypeExpression extends CompoundKeepExpression {

  /**
   * The {@code KeepSuperTypeBuilder} enables the construction of new {@code KeepSuperTypeExpression}
   * instances through a nice and fluent interface.
   *
   * @author Rico Bergmann
   */
  public static class KeepSuperTypeBuilder extends CompoundKeepBuilder {

    private ClassResource typeToKeep;

    /**
     * Default constructor.
     */
    private KeepSuperTypeBuilder() {
      this.keeps = new ArrayList<>();
    }

    /**
     * Specifies the supertype that should be instantiated.
     */
    @Nonnull
    public KeepSuperTypeBuilder supertype(@Nonnull ClassResource type) {
      this.typeToKeep = type;
      return this;
    }

    /**
     * Finishes the construction process.
     */
    @Nonnull
    public KeepSuperTypeExpression buildExpression() {
      Assert.noNullArguments("All components must be specified", typeToKeep, target, keeps);
      return new KeepSuperTypeExpression(typeToKeep, target, keeps);
    }

  }

  /**
   * Starts the creation process for a new {@code KeepSuperTypeExpression}.
   */
  @Nonnull
  public static KeepSuperTypeBuilder keep() {
    return new KeepSuperTypeBuilder();
  }

  @Nonnull
  private final ClassResource typeToKeep;
  
  @Nonnull
  private final ClassResource target;

  /**
   * Full constructor.
   *
   * @param typeToKeep the supertype that should be instantiated.
   * @param target the name of the view that should be generated for all matching instances
   * @param keeps the keep statements that should form the attributes of the generated view
   *     instances
   */
  public KeepSuperTypeExpression(
      @Nonnull ClassResource typeToKeep,
      @Nonnull ClassResource target,
      @Nonnull List<KeepExpression> keeps) {
    this.typeToKeep = typeToKeep;
    this.target = target;
    this.keeps = keeps;
  }

  /**
   * Provides the supertype that should be instantiated.
   */
  @Nonnull
  public ClassResource getType() {
    return typeToKeep;
  }

  /**
   * Provides the name of the view that should be generated for all matching instances.
   */
  @Nonnull
  public ClassResource getTarget() {
    return target;
  }

  @Override
  public void accept(@Nonnull KeepExpressionVisitor visitor) {
    visitor.visit(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeepSuperTypeExpression that = (KeepSuperTypeExpression) o;
    return typeToKeep.equals(that.typeToKeep) &&
        target.equals(that.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(typeToKeep, target);
  }

  @Override
  public String toString() {
    return "keep supertype " + typeToKeep + " as " + target;
  }
}
