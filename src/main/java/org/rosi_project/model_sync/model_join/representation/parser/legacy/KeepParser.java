package org.rosi_project.model_sync.model_join.representation.parser.legacy;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAttributesExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepExpression;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParsingException;

/**
 * The {@code KeepParser} takes care of reading the {@code keep statements} and creating the
 * corresponding {@link KeepExpression}s for the body of an {@code join statement}.
 * <p>
 * This class is primarily intended to be created and invoked by the {@link JoinParser} only as will
 * modify the state of that as well.
 *
 * @author Rico Bergmann
 */
@Deprecated
class KeepParser {

  private static final String END_OF_JOIN_BLOCK_DELIMITER = "}";
  private static final Pattern KEEP_ATTRIBUTE_DECLARATION = Pattern
      .compile(".*keep attributes\\W*(?<attribute>[\\w\\\\.]*)$");

  @Nonnull
  private final String startingLine;

  @Nonnull
  private final BufferedReader modelReader;

  private boolean hasReadEndOfJoinBlockDelimiter = false;

  /**
   * Full constructor.
   * @param startingLine the line that was last read by the {@link JoinParser} and should
   *     therefore become the starting point for {@code this} parser.
   * @param modelReader the reader that provides access to the following lines in the {@code
   *     ModelJoin} file currently being read.
   */
  KeepParser(@Nonnull String startingLine, @Nonnull BufferedReader modelReader) {
    this.startingLine = startingLine;
    this.modelReader = modelReader;
  }

  /**
   * Creates the {@link KeepExpression}s for the current {@code join statement}.
   * <p>
   * If something goes wrong, either an {@code IOException} will be thrown directly or a {@link
   * ModelJoinParsingException} is used.
   */
  @Nonnull
  public Collection<KeepExpression> run() throws IOException {
    List<KeepExpression> detectedKeeps = new LinkedList<>();
    Matcher keepMatcher = KEEP_ATTRIBUTE_DECLARATION.matcher(startingLine);

    parseLine(startingLine, keepMatcher).ifPresent(detectedKeeps::add);

    String currentLine;
    while (!hasReadEndOfJoinBlockDelimiter && (currentLine = modelReader.readLine()) != null) {
      keepMatcher = KEEP_ATTRIBUTE_DECLARATION.matcher(currentLine);
      parseLine(currentLine, keepMatcher).ifPresent(detectedKeeps::add);
    }

    return detectedKeeps;
  }

  /**
   * Tries to create a {@link KeepExpression} for a single {@code keep statement} written on the
   * {@code current line}.
   * <p>
   * If there is no such statement, an empty {@code Optional} will be returned.
   * <p>
   * If the line indicates the end of the keep-block, the state of {@code this} parser will be
   * updated accordingly.
   */
  @Nonnull
  private Optional<KeepExpression> parseLine(String currentLine, Matcher keepMatcher) {
    Optional<KeepExpression> result = Optional.empty();
    if (keepMatcher.matches()) {
      AttributePath attribute = AttributePath.fromQualifiedPath(keepMatcher.group("attribute"));
      result = Optional.of(KeepAttributesExpression.keepAttributes(attribute));
    }

    if (currentLine.contains(END_OF_JOIN_BLOCK_DELIMITER)) {
      hasReadEndOfJoinBlockDelimiter = true;
    }
    return result;
  }

}
