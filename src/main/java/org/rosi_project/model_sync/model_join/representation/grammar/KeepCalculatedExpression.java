package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.OCLStatement;
import org.rosi_project.model_sync.model_join.representation.core.TypedAttributePath;

/**
 * A {@code KeepCalculatedExpression} adds an attribute to some join by evaluating an arbitrary
 * {@link OCLStatement OCL statement}.
 * <p>
 * This is the most general kind of {@link KeepExpression} as all other expressions may be expressed
 * in OCL as well.
 *
 * @author Rico Bergmann
 * @see OCLStatement
 */
public class KeepCalculatedExpression extends KeepExpression {

  /**
   * The {@code KeepCalculatedBuilder} enables the construction of new {@link
   * KeepCalculatedExpression}s through a nice and fluent interface.
   *
   * @author Rico Bergmann
   */
  public static class KeepCalculatedBuilder {

    @Nonnull
    private final OCLStatement calculationRule;

    /**
     * Full constructor.
     *
     * @param calculationRule the OCL statement that should be used to calculate the attribute's
     *     value.
     */
    KeepCalculatedBuilder(@Nonnull OCLStatement calculationRule) {
      this.calculationRule = calculationRule;
    }

    /**
     * Specifies the name of the attribute in the target join.
     */
    @Nonnull
    public KeepCalculatedExpression as(@Nonnull TypedAttributePath target) {
      return new KeepCalculatedExpression(calculationRule, target);
    }
  }

  /**
   * Generates a new {@code keep calculated} expression.
   */
  @Nonnull
  public static KeepCalculatedBuilder keepCalculatedAttribute(
      @Nonnull OCLStatement calculationRule) {
    return new KeepCalculatedBuilder(calculationRule);
  }

  @Nonnull
  private final OCLStatement calculationRule;

  @Nonnull
  private final TypedAttributePath target;

  /**
   * Full constructor.
   *
   * @param calculationRule the OCL statement that should be used to calculate the attribute's
   *     value
   * @param target the name of the attribute under which the aggregation result should be
   *     available in the join
   */
  public KeepCalculatedExpression(
      @Nonnull OCLStatement calculationRule,
      @Nonnull TypedAttributePath target) {
    this.calculationRule = calculationRule;
    this.target = target;
  }

  /**
   * Provides the OCL expression that is used to determine the value of {@code this} attribute.
   */
  @Nonnull
  public OCLStatement getCalculationRule() {
    return calculationRule;
  }

  /**
   * Provides the attribute under which the result of {@code this} calculation should be made
   * available.
   */
  @Nonnull
  public TypedAttributePath getTarget() {
    return target;
  }

  @Override
  public void accept(@Nonnull KeepExpressionVisitor visitor) {
    visitor.visit(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeepCalculatedExpression that = (KeepCalculatedExpression) o;
    return calculationRule.equals(that.calculationRule) &&
        target.equals(that.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(calculationRule, target);
  }

  @Override
  public String toString() {
    return "keep calculated attribute " + calculationRule + " as " + target;
  }
}
