package org.rosi_project.model_sync.model_join.representation.core;

import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * A {@code RelativeAttribute} represents a member of some class.
 * <p>
 * In difference to the {@link AttributePath} this class does not retain any information about the
 * class to which it belongs.
 *
 * @author Rico Bergmann
 */
public class RelativeAttribute {

  /**
   * Extracts the attribute from a qualified {@link AttributePath}.
   */
  @Nonnull
  public static RelativeAttribute from(@Nonnull AttributePath attributePath) {
    return new RelativeAttribute(attributePath.getAttributeName());
  }

  /**
   * Generates a new {@code RelativeAttribute} with the given name.
   */
  @Nonnull
  public static RelativeAttribute of(@Nonnull String attributeName) {
    if (!attributeName.matches(ATTRIBUTE_NAME_REGEX)) {
      throw new IllegalArgumentException("Not a valid attribute name: " + attributeName);
    }
    return new RelativeAttribute(attributeName);
  }

  private static final String ATTRIBUTE_NAME_REGEX = "^[a-zA-Z_]\\w*$";

  @Nonnull
  private final String attributeName;

  /**
   * Full constructor.
   *
   * @param attributeName the name of the attribute to generate
   */
  public RelativeAttribute(@Nonnull String attributeName) {
    this.attributeName = attributeName;
  }

  /**
   * Provides the name of the represented attribute.
   */
  @Nonnull
  public String getAttributeName() {
    return attributeName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelativeAttribute that = (RelativeAttribute) o;
    return attributeName.equals(that.attributeName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attributeName);
  }

  @Override
  public String toString() {
    return attributeName;
  }
}
