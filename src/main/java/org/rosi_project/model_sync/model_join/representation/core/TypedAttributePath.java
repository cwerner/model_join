package org.rosi_project.model_sync.model_join.representation.core;

import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * A {@code TypedAttributePath} extends a default {@link AttributePath} by adding type information,
 * i.e. adding information about the type of the object the path references.
 * <p>
 * As we do not have any knowledge about the actual classes that are represented by paths we may
 * not validate, whether the given type indeed matches the attribute's type.
 *
 * @author Rico Bergmann
 */
public class TypedAttributePath extends AttributePath {

  private static final String PATH_TYPE_SEPARATOR = ":";

  /**
   * Creates a new {@code TypedAttributePath} by combining the provided path and type information.
   */
  @Nonnull
  public static TypedAttributePath of(@Nonnull AttributePath path, @Nonnull ClassResource type) {
    return new TypedAttributePath(path, type);
  }

  /**
   * Generates a new {@code TypedAttributePath} by splitting up the given path into its path and
   * type portion. The {@code qualifiedTypedPath} is expected to match the format {@code path:type},
   * where {@code path} may be parsed using {@link AttributePath#fromQualifiedPath(String)} and type
   * may be parsed using {@link ClassResource#fromQualifiedName(String)}.
   */
  @Nonnull
  public static TypedAttributePath fromQualifiedTypedPath(@Nonnull String qualifiedTypedPath) {
    final int pathTypeSplitPos = qualifiedTypedPath.lastIndexOf(PATH_TYPE_SEPARATOR);

    if (pathTypeSplitPos < 0) {
      throw new IllegalArgumentException("Missing type portion on '" + qualifiedTypedPath + "'");
    }

    final String qualifiedPath = qualifiedTypedPath.substring(0, pathTypeSplitPos);
    final String type = qualifiedTypedPath.substring(pathTypeSplitPos + 1);
    return new TypedAttributePath(AttributePath.fromQualifiedPath(qualifiedPath), ClassResource.fromQualifiedName(type));
  }

  @Nonnull
  private final ClassResource type;

  /**
   * Full constructor.
   *
   * @param path the attribute
   * @param type the attribute's type
   */
  public TypedAttributePath(
      @Nonnull AttributePath path,
      @Nonnull ClassResource type) {
    super(path);
    this.type = type;
  }

  /**
   * Provides the type of {@code this} attribute.
   */
  @Nonnull
  public ClassResource getType() {
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TypedAttributePath)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    TypedAttributePath that = (TypedAttributePath) o;
    return type.equals(that.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), type);
  }

  @Override
  public String toString() {
    return super.toString() + PATH_TYPE_SEPARATOR + type.toString();
  }

}
