package org.rosi_project.model_sync.model_join.representation.util;

import javax.annotation.Nonnull;

/**
 * Utility class to provide a number of useful assertion mechanisms.
 * <p>
 * On failure, an {@code AssertionError} with a dedicated message will be thrown.
 *
 * @author Rico Bergmann
 */
public class Assert {

  /**
   * Ensures that an object is not {@code null}.
   */
  public static void notNull(Object obj, @Nonnull String failureMessage) {
    if (obj == null) {
      throw new AssertionError(failureMessage);
    }
  }

  /**
   * Ensures that none of the arguments is {@code null}.
   */
  public static void noNullArguments(@Nonnull String failureMessage, Object... arguments) {
    if (arguments == null) {
      throw new AssertionError("Vararg arguments may not be null");
    }
    for (Object arg : arguments) {
      if (arg == null) {
        throw new AssertionError(failureMessage);
      }
    }
  }
}
