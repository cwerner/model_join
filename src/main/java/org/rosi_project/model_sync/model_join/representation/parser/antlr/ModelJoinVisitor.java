package org.rosi_project.model_sync.model_join.representation.parser.antlr;

import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinBaseVisitor;
import org.rosi_project.model_sync.model_join.representation.parser.antlr.generated.ModelJoinParser.ModeljoinContext;
import org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder;

/**
 * The {@code ModelJoinVisitor} creates a {@link ModelJoinExpression} instances from a parsed {@code
 * ModelJoin} file.
 *
 * @author Rico Bergmann
 */
public class ModelJoinVisitor extends ModelJoinBaseVisitor<ModelJoinExpression> {

  @Override
  public ModelJoinExpression visitModeljoin(ModeljoinContext ctx) {
    JoinStatementVisitor joinVisitor = new JoinStatementVisitor();
    ModelJoinBuilder modelJoinBuilder = ModelJoinBuilder.createNewModelJoin();

    ctx.join().stream() //
        .map(joinVisitor::visitJoin) //
        .forEach(modelJoinBuilder::add);

    return modelJoinBuilder.build();
  }

}
