package org.rosi_project.model_sync.model_join.representation.writer;

import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;

/**
 * A {@code ModelJoinWritingService} transforms a {@link ModelJoinExpression model join description}
 * into some different representation.
 *
 * @author Rico Bergmann
 */
public interface ModelJoinWritingService<T> {

  /**
   * Performs the transformation. The result of this method depends on the actual details of the
   * writing service and should be documented there.
   */
  T write(@Nonnull ModelJoinExpression modelJoin);

}
