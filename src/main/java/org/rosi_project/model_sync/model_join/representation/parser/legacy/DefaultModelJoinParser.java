package org.rosi_project.model_sync.model_join.representation.parser.legacy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.parser.ModelJoinParsingException;
import org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder;

/**
 * The {@code DefaultModelJoinParser} reads a {@code ModelJoin} file from disk and creates matching
 * representation for it.
 *
 * @author Rico Bergmann
 */
@Deprecated
public class DefaultModelJoinParser {

  /**
   * Converts the {@code ModelJoin} file into a {@link ModelJoinExpression}, wrapped into an {@code
   * Optional}. If parsing fails, an empty {@code Optional} will be returned.
   */
  @Nonnull
  public static Optional<ModelJoinExpression> read(@Nonnull File modelFile) {
    DefaultModelJoinParser parser = new DefaultModelJoinParser(modelFile, ErrorReportingStrategy.OPTIONAL);
    return parser.run();
  }

  /**
   * Converts the {@code ModelJoin} file into a {@link ModelJoinExpression}. If parsing fails, a
   * {@link ModelJoinParsingException} will be thrown.
   *
   * @throws ModelJoinParsingException if the model may not be parsed for some reason.
   */
  @Nonnull
  public static ModelJoinExpression readOrThrow(@Nonnull File modelFile) {
    DefaultModelJoinParser parser = new DefaultModelJoinParser(modelFile, ErrorReportingStrategy.EXCEPTION);
    Optional<ModelJoinExpression> result = parser.run();

    if (result.isPresent()) {
      return result.get();
    } else {
      /*
       * Theoretically, the parser should throw the exception by itself, so this code should never
       * actually run. It is merely here for safety purposes (unchecked access of Optional.get())
       */
      throw new ModelJoinParsingException("Result not present but no exception was thrown either");
    }
  }

  /**
   * Indicates what should happen if parsing fails.
   */
  enum ErrorReportingStrategy {

    /**
     * On failure an empty {@code Optional} should be returned.
     */
    OPTIONAL,

    /**
     * On failure an {@link ModelJoinParsingException} should be thrown.
     */
    EXCEPTION
  }

  private static final Pattern EMPTY_LINE = Pattern.compile("^\\s*$");
  private static final Pattern IMPORT_STATEMENT = Pattern.compile("^import .*");
  private static final Pattern TARGET_STATEMENT = Pattern.compile("^target .*");
  private static final Pattern JOIN_STATEMENT = Pattern
      .compile("^(((left|right) outer)|theta|natural) join .*");

  @Nonnull
  private final File modelFile;

  @Nonnull
  private final ErrorReportingStrategy errorReportingStrategy;

  /**
   * Full constructor.
   *
   * @param modelFile the file to parse
   * @param errorReportingStrategy what to do in case an error occurs.
   */
  private DefaultModelJoinParser(@Nonnull File modelFile,
      @Nonnull ErrorReportingStrategy errorReportingStrategy) {
    this.modelFile = modelFile;
    this.errorReportingStrategy = errorReportingStrategy;
  }

  /**
   * Creates the {@code ModelJoin} representation from the {@code modelFile}.
   * <p>
   * Depending on the selected {@code errorReportingStrategy} either an Exception will be thrown, or
   * an empty {@code Optional} will be returned if something goes wrong.
   */
  private Optional<ModelJoinExpression> run() {
    ModelJoinExpression resultingModel;
    try (FileInputStream modelInputStream = new FileInputStream(modelFile);
        InputStreamReader modelInputReader = new InputStreamReader(modelInputStream);
        BufferedReader bufferedModelReader = new BufferedReader(modelInputReader)) {

      ModelJoinBuilder modelJoinBuilder = ModelJoinBuilder.createNewModelJoin();
      readModelFileAndPopulateBuilder(bufferedModelReader, modelJoinBuilder);
      resultingModel = modelJoinBuilder.build();

    } catch (ModelJoinParsingException e) {
      switch (errorReportingStrategy) {
        case OPTIONAL:
          return Optional.empty();
        case EXCEPTION:
          throw e;
        default:
          throw new AssertionError(errorReportingStrategy);
      }
    } catch (IOException e) {
      switch (errorReportingStrategy) {
        case OPTIONAL:
          return Optional.empty();
        case EXCEPTION:
          throw new ModelJoinParsingException(e);
        default:
          throw new AssertionError(errorReportingStrategy);
      }
    }
    return Optional.of(resultingModel);
  }

  /**
   * Reads the {@code modelFile} and constructs the corresponding {@code ModelJoin} on the fly.
   * <p>
   * On error, an {@code IOException} or an {@link ModelJoinParsingException} will be thrown.
   *
   * @throws ModelJoinParsingException if the model file's content are malformed or another
   *     component failed
   * @throws IOException if the model file might not be read any further
   */
  private void readModelFileAndPopulateBuilder(BufferedReader modelReader,
      ModelJoinBuilder modelBuilder) throws IOException {
    String currentLine;
    while ((currentLine = modelReader.readLine()) != null) {
      if (lineShouldBeSkipped(currentLine)) {
        // the line should be skipped. So we do nothing.
        continue;
      } else if (lineStartsJoinDeclaration(currentLine)) {
        // a new join is being declared. Delegate to the join parser to handle the rest.
        JoinParser joinParser = new JoinParser(currentLine, modelReader);
        modelBuilder.add(joinParser.run());
      } else {
        // we do not know what to do with the current line. Should abort.
        throw new ModelJoinParsingException("Unexpected line: '" + currentLine + "'");
      }
    }
  }

  /**
   * Checks, whether the parser should consider the given {@code line} any further, or just ignore
   * it.
   * <p>
   * A line should be skipped, iff.
   * <ul>
   * <li>It only consists of whitespace characters</li>
   * <li>It is an {@code import statement} (as those are not yet considered in the current {@code
   * ModelJoin} abstraction)</li>
   * <li>It is a {@code target statement} (as those are not yet considered in the current {@code
   * ModelJoin} abstraction)</li>
   * </ul>
   */
  private boolean lineShouldBeSkipped(String line) {
    return EMPTY_LINE.matcher(line).matches() //
        || IMPORT_STATEMENT.matcher(line).matches() //
        || TARGET_STATEMENT.matcher(line).matches();
  }

  /**
   * Checks, whether the given {@code line} is the beginning of a {@code join statement}.
   */
  private boolean lineStartsJoinDeclaration(String line) {
    return JOIN_STATEMENT.matcher(line).matches();
  }
}
