package org.rosi_project.model_sync.model_join.representation.util;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepAttributesExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepReferenceExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepReferenceExpression.KeepReferenceBuilder;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSubTypeExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSubTypeExpression.KeepSubTypeBuilder;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSuperTypeExpression;
import org.rosi_project.model_sync.model_join.representation.grammar.KeepSuperTypeExpression.KeepSuperTypeBuilder;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.NaturalJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.OuterJoinBuilder;
import org.rosi_project.model_sync.model_join.representation.util.JoinFactory.ThetaJoinBuilder;

/**
 * The {@code ModelJoinBuilder} provides a nice and fluent way of creating new {@code ModelJoin}
 * instances.
 *
 * @author Rico Bergmann
 */
public class ModelJoinBuilder {

  public static ThetaJoinBuilder thetaJoin() {
    return JoinFactory.createNew().theta();
  }
  
  public static NaturalJoinBuilder naturalJoin() {
	  return JoinFactory.createNew().natural();
  }
  
  public static OuterJoinBuilder outerJoin() {
	  return JoinFactory.createNew().outer();
  }

  public static KeepAttributesExpression attributes(AttributePath firstAttribute, AttributePath... moreAttributes) {
    return KeepAttributesExpression.keepAttributes(firstAttribute, moreAttributes);
  }

  public static KeepReferenceBuilder outgoing(AttributePath attr) {
    return KeepReferenceExpression.keep().outgoing(attr);
  }
  
  public static KeepReferenceBuilder incoming(AttributePath attr) {
	return KeepReferenceExpression.keep().incoming(attr);
  }

  public static KeepSuperTypeBuilder supertype(ClassResource supertype) {
    return KeepSuperTypeExpression.keep().supertype(supertype);
  }
  
  public static KeepSubTypeBuilder subtype(ClassResource subtype) {
	return KeepSubTypeExpression.keep().subtype(subtype);
  }

  /**
   * Starts the construction process for a new {@link ModelJoinExpression}.
   */
  @Nonnull
  public static ModelJoinBuilder createNewModelJoin() {
    return new ModelJoinBuilder();
  }

  private List<JoinExpression> joinBuffer = new LinkedList<>();

  /**
   * Full/default constructor.
   */
  private ModelJoinBuilder() {
    // pass
  }

  /**
   * Extends the current {@code ModelJoin} by another join.
   */
  @Nonnull
  public ModelJoinBuilder add(@Nonnull JoinExpression join) {
    joinBuffer.add(join);
    return this;
  }

  /**
   * Finishes the construction process and provides the resulting {@code ModelJoin}.
   */
  @Nonnull
  public ModelJoinExpression build() {
    return new ModelJoinExpression(joinBuffer);
  }
}
