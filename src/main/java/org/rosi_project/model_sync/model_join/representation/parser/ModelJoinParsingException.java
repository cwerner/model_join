package org.rosi_project.model_sync.model_join.representation.parser;

/**
 * The {@code ModelJoinParsingException} indicates that a {@code ModelJoin} file could not be read
 * correctly, e.g. because of missing values or a malformed structure.
 *
 * @author Rico Bergmann
 */
public class ModelJoinParsingException extends RuntimeException {

  /**
   * @see RuntimeException#RuntimeException(String)
   */
  public ModelJoinParsingException(String message) {
    super(message);
  }

  /**
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public ModelJoinParsingException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * @see RuntimeException#RuntimeException(Throwable)
   */
  public ModelJoinParsingException(Throwable cause) {
    super(cause);
  }
}
