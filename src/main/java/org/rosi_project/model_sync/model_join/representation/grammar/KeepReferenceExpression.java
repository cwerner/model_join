package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.util.Assert;

// TODO the Xtext grammar also specifies an optional `as reference` part - what does that part do?
// most likely `as reference` will refer to another type that was already defined in its dedicated
// join statement.

/**
 * A {@code KeepReferenceExpression} retains links to other instances in the original models. All
 * objects that are referenced in the base models will also be available in the resulting ModelJoin
 * views.
 * <p>
 * As references are directional, each expression also distinguishes between {@code outgoing} and
 * {@code incoming} references - depending on whether the model class of {@code this} expression
 * owns the reference ({@code outgoing}) or instances of this model class are being referenced
 * ({@code incoming}).
 * <p>
 * The referenced instances will be made available according to a number of keep statements which
 * specify the attributes to include. This boils down to the creation of a nested ModelJoin view for
 * these instances.
 *
 * @author Rico Bergmann
 */
public class KeepReferenceExpression extends CompoundKeepExpression {

  /**
   * The {@code ReferenceDirection} defines whether a reference is owned by the containing model
   * class or whether it is the class being referenced.
   */
  public enum ReferenceDirection {

    /**
     * Indicates that the containing model class owns the reference.
     */
    OUTGOING,

    /**
     * Indicates that the containing model class is referenced by some other class.
     */
    INCOMING
  }

  /**
   * The {@code KeepReferenceBuilder} enables the construction of new {@code
   * KeepReferenceExpression} instances through a nice and fluent interface.
   *
   * @author Rico Bergmann
   */
  public static class KeepReferenceBuilder extends CompoundKeepBuilder {

    private ReferenceDirection referenceDirection;
    private AttributePath attribute;

    /**
     * Default constructor.
     */
    private KeepReferenceBuilder() {
      this.keeps = new ArrayList<>();
    }

    /**
     * Creates an outgoing reference for the given attribute.
     */
    @Nonnull
    public KeepReferenceBuilder outgoing(@Nonnull AttributePath attribute) {
      this.referenceDirection = ReferenceDirection.OUTGOING;
      this.attribute = attribute;
      return this;
    }

    /**
     * Creates an incoming reference for the given attribute.
     */
    @Nonnull
    public KeepReferenceBuilder incoming(@Nonnull AttributePath attribute) {
      this.referenceDirection = ReferenceDirection.INCOMING;
      this.attribute = attribute;
      return this;
    }

    /**
     * Finishes the construction process.
     */
    @Nonnull
    public KeepReferenceExpression buildExpression() {
      Assert.noNullArguments("All components must be specified", attribute, referenceDirection, target, keeps);
      return new KeepReferenceExpression(attribute, referenceDirection, target, keeps);
    }
  }

  /**
   * Starts the creation process for a new {@code KeepReferenceExpression}.
   */
  @Nonnull
  public static KeepReferenceBuilder keep() {
    return new KeepReferenceBuilder();
  }

  @Nonnull
  private final AttributePath attribute;

  @Nonnull
  private final ReferenceDirection referenceDirection;
  
  @Nonnull
  private final ClassResource target;

  /**
   * Full constructor.
   *
   * @param attribute the attribute of the source model which contains the references
   * @param referenceDirection whether the source model owns the reference or is being
   *     referenced
   * @param target the name of the class which enca
   * @param keeps the statements which should be build the "nested" view for the references
   */
  public KeepReferenceExpression(
      @Nonnull AttributePath attribute,
      @Nonnull ReferenceDirection referenceDirection,
      @Nonnull ClassResource target,
      @Nonnull List<KeepExpression> keeps) {
    this.attribute = attribute;
    this.referenceDirection = referenceDirection;
    this.target = target;
    this.keeps = keeps;
  }

  /**
   * Provides the attribute of the source model which contains the references.
   */
  @Nonnull
  public AttributePath getAttribute() {
    return attribute;
  }

  /**
   * Provides the kind of reference, that is whether the containing Join owns the reference or is
   * being referenced by another model class or Join.
   */
  @Nonnull
  public ReferenceDirection getReferenceDirection() {
    return referenceDirection;
  }

  /**
   * Provides the name of the attribute under which the referenced instances should be made
   * available.
   */
  @Nonnull
  public ClassResource getTarget() {
    return target;
  }

  @Override
  public void accept(@Nonnull KeepExpressionVisitor visitor) {
    visitor.visit(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeepReferenceExpression that = (KeepReferenceExpression) o;
    return attribute.equals(that.attribute) && target.equals(that.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attribute, target);
  }

  @Override
  public String toString() {
    String directionString = referenceDirection.toString().toLowerCase();
    return "keep " + directionString + " " + attribute + " as type " + target;
  }
}
