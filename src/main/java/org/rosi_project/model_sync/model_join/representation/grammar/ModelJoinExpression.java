package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * The {@code ModelJoinExpression} represents an actual ModelJoin, i.e. a specification of which
 * joins to use and how they should be built.
 *
 * @author Rico Bergmann
 */
public class ModelJoinExpression implements Iterable<JoinExpression> {
	
  private String name = "";
  
  public String getName() {
	  return name;
  }
  
  public void setName(String n) {
	  name = n;
  }

  /**
   * Creates a new {@code ModelJoin}.
   *
   * @param joins the joins that should be included.
   */
  @Nonnull
  public static ModelJoinExpression with(@Nonnull List<JoinExpression> joins) {
    return new ModelJoinExpression(joins);
  }

  /**
   * Ensures that all joins specify distinct join targets and throws an {@code
   * IllegalArgumentException} otherwise.
   */
  private static void assertDifferentTargetsForAllJoins(Collection<JoinExpression> joinsToCheck) {
    final long totalNumberOfJoins = joinsToCheck.size();
    final long numberOfDistinctTargetsSpecified = joinsToCheck.stream()//
        .map(JoinExpression::getTarget) //
        .distinct() //
        .count();

    if (numberOfDistinctTargetsSpecified < totalNumberOfJoins) {
      throw new IllegalArgumentException("Some joins share the same target");
    }
  }

  @Nonnull
  private final List<JoinExpression> joins;

  /**
   * Full constructor.
   *
   * @param joins the joins that this ModelJoin consists of
   */
  public ModelJoinExpression(List<JoinExpression> joins) {
    assertDifferentTargetsForAllJoins(joins);
    this.joins = joins;
  }

  /**
   * Provides all joins {@code this} model specifies.
   */
  @Nonnull
  public Collection<JoinExpression> getJoins() {
    return new ArrayList<>(joins);
  }

  @Nonnull
  @Override
  public Iterator<JoinExpression> iterator() {
    return joins.iterator();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ModelJoinExpression that = (ModelJoinExpression) o;
    return joins.equals(that.joins);
  }

  @Override
  public int hashCode() {
    return Objects.hash(joins);
  }

  @Override
  public String toString() {
    return joins.toString();
  }
}
