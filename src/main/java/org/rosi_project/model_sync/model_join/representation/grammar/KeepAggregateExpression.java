package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.RelativeAttribute;

/**
 * A {@code KeepAggregateExpression} adds an attribute to some join by calculating an aggregation on
 * the referenced collection of numerical values. The possible aggregation operations are equal to
 * those provided by the SQL programming language.
 * <p>
 * The aggregate's result will in turn be some numerical value.
 *
 * @author Rico Bergmann
 */
public class KeepAggregateExpression extends KeepExpression {

  /**
   * The {@code AggregationType} defines what aggregation should be performed.
   */
  public enum AggregationType {

    /**
     * The attribute's value should be the sum of all the referenced values.
     */
    SUM,

    /**
     * The attribute's value should be the average value of all the referenced values.
     */
    AVG,

    /**
     * The attribute's value should be the minimum value of all the referenced values.
     */
    MIN,

    /**
     * The attribute's value should be the maximum value of all the referenced values.
     */
    MAX,

    /**
     * The attribute's value should be the number of referenced elements.
     */
    SIZE
  }

  /**
   * The {@code AggregateBuilder} enables the construction of new {@link KeepAggregateExpression}s
   * through a nice and fluent interface.
   *
   * @author Rico Bergmann
   */
  public static class AggregateBuilder {

    @Nonnull
    private final AggregationType aggregationType;

    @Nonnull
    private final RelativeAttribute aggregatedAttribute;

    @Nonnull
    private AttributePath source;

    /**
     * Full constructor.
     *
     * @param aggregationType the aggregation operation that should be performed
     * @param aggregatedAttribute the attribute on which the aggregation should be performed
     */
    AggregateBuilder(@Nonnull AggregationType aggregationType,
        @Nonnull RelativeAttribute aggregatedAttribute) {
      this.aggregationType = aggregationType;
      this.aggregatedAttribute = aggregatedAttribute;
    }

    /**
     * Specifies the reference destination that provides the attribute to join.
     * <p>
     * Suppose the following example:
     * <pre>
     * {@code
     *  class City {
     *    List<Street> streets;
     *  }
     *
     *  class Street {
     *    int length;
     *  }
     * }
     * </pre>
     * If one was to calculate the average length of all streets per city, the following aggregation
     * could be used: {@code keep aggregate avg(length) over City.streets as avgStreetLength}. In
     * this case, {@code City.streets} is the wanted reference destination as it provides the {@code
     * length} attribute which should be used for the aggregation.
     */
    @Nonnull
    public AggregateBuilder over(@Nonnull AttributePath source) {
      this.source = source;
      return this;
    }

    /**
     * Specifies the name of the aggregation attribute in the target join.
     */
    @Nonnull
    public KeepAggregateExpression as(@Nonnull AttributePath target) {
      return new KeepAggregateExpression(aggregationType, aggregatedAttribute, source, target);
    }
  }

  /**
   * Generates a new {@code keep aggregate} expression that sums up the referenced values.
   */
  @Nonnull
  public static AggregateBuilder sum(@Nonnull RelativeAttribute attribute) {
    return new AggregateBuilder(AggregationType.SUM, attribute);
  }

  /**
   * Generates a new {@code keep aggregate} expression that calculates the average value of the
   * referenced values.
   */
  @Nonnull
  public static AggregateBuilder avg(@Nonnull RelativeAttribute attribute) {
    return new AggregateBuilder(AggregationType.AVG, attribute);
  }

  /**
   * Generates a new {@code keep aggregate} expression that provides the minimum value of the
   * referenced values.
   */
  @Nonnull
  public static AggregateBuilder min(@Nonnull RelativeAttribute attribute) {
    return new AggregateBuilder(AggregationType.MIN, attribute);
  }

  /**
   * Generates a new {@code keep aggregate} expression that provides the maximum value of the
   * referenced values.
   */
  @Nonnull
  public static AggregateBuilder max(@Nonnull RelativeAttribute attribute) {
    return new AggregateBuilder(AggregationType.MAX, attribute);
  }

  /**
   * Generates a new {@code keep aggregate} expression that counts the number of values that are
   * referenced.
   */
  @Nonnull
  public static AggregateBuilder size(@Nonnull RelativeAttribute attribute) {
    return new AggregateBuilder(AggregationType.SIZE, attribute);
  }

  @Nonnull
  private final AggregationType aggregation;

  @Nonnull
  private final RelativeAttribute aggregatedAttribute;

  @Nonnull
  private final AttributePath source;

  @Nonnull
  private final AttributePath target;

  /**
   * Full constructor.
   *
   * @param aggregation the aggregation operation that should be performed
   * @param aggregatedAttribute the attribute on which the aggregation should be performed
   * @param source the field which provides the {@code aggregatedAttribute}. Therefore this
   *     attribute has to be multi-valued.
   * @param target the name of the attribute under which the aggregation result should be
   *     available in the join
   */
  public KeepAggregateExpression(
      @Nonnull AggregationType aggregation,
      @Nonnull RelativeAttribute aggregatedAttribute,
      @Nonnull AttributePath source,
      @Nonnull AttributePath target) {
    this.aggregation = aggregation;
    this.aggregatedAttribute = aggregatedAttribute;
    this.source = source;
    this.target = target;
  }

  /**
   * Provides the aggregation operation {@code this} expression will perform.
   */
  @Nonnull
  public AggregationType getAggregation() {
    return aggregation;
  }

  /**
   * Provides the attribute that will be aggregated. As all {@link AggregationType aggregation
   * operations} are defined on numerical values, this attribute in turn has to be of numerical
   * type.
   */
  @Nonnull
  public RelativeAttribute getAggregatedAttribute() {
    return aggregatedAttribute;
  }

  /**
   * Provides the field which contains the {@link #getAggregatedAttribute() aggregated attribute}.
   * As an aggregation will - by definition - be performed upon an arbitrary number of values, this
   * attribute has to be some kind of collection.
   */
  @Nonnull
  public AttributePath getSource() {
    return source;
  }

  /**
   * Provides the attribute under which the result of {@code this} aggregation should be made
   * available.
   */
  @Nonnull
  public AttributePath getTarget() {
    return target;
  }

  @Override
  public void accept(@Nonnull KeepExpressionVisitor visitor) {
    visitor.visit(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeepAggregateExpression that = (KeepAggregateExpression) o;
    return aggregation == that.aggregation &&
        aggregatedAttribute.equals(that.aggregatedAttribute) &&
        source.equals(that.source) &&
        target.equals(that.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(aggregation, aggregatedAttribute, source, target);
  }

  @Override
  public String toString() {
    String aggregationOperation = aggregation.toString().toLowerCase();
    return "keep aggregate " //
        + aggregationOperation + "(" + aggregatedAttribute + ") " //
        + "over" + source //
        + " as " + target;
  }
}
