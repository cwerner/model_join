package org.rosi_project.model_sync.model_join.representation.util;

public final class Nothing {

  public static Nothing __() {
    return new Nothing();
  }

  private Nothing() {};

}
