package org.rosi_project.model_sync.model_join.representation.grammar;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;

/**
 * A {@code KeepAttributesExpression} simply retains a number of attributes from arbitrary source
 * classes and includes them in the target join. The attributes' name and type will be left
 * unchanged.
 *
 * @author Rico Bergmann
 */
public class KeepAttributesExpression extends KeepExpression {

  /**
   * Generates a new keep expression for a number of attributes.
   */
  @Nonnull
  public static KeepAttributesExpression keepAttributes(@Nonnull AttributePath firstAttribute,
      @Nonnull AttributePath... moreAttributes) {
    List<AttributePath> attributes = Lists.newArrayList(moreAttributes);
    attributes.add(0, firstAttribute);
    return new KeepAttributesExpression(attributes);
  }

  @Nonnull
  private final List<AttributePath> attributes;

  /**
   * Full constructor.
   *
   * @param attributes the attributes for which the expression should be created
   */
  public KeepAttributesExpression(@Nonnull List<AttributePath> attributes) {
    this.attributes = attributes;
  }

  /**
   * Provides the attributes that should be retained in the join.
   * <p>
   * The returned {@code List} is a shallow copy of the attributes in {@code this} expression.
   */
  @Nonnull
  public List<AttributePath> getAttributes() {
    return new ArrayList<>(attributes);
  }

  @Override
  public void accept(@Nonnull KeepExpressionVisitor visitor) {
    visitor.visit(this);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeepAttributesExpression that = (KeepAttributesExpression) o;
    return this.attributes.equals(that.attributes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attributes);
  }

  @Override
  public String toString() {
    return "keep attributes " + attributes.stream() //
        .map(AttributePath::toString) //
        .reduce((firstAttribute, secondAttribute) -> firstAttribute + ", " + secondAttribute) //
        .orElseThrow(() -> new IllegalStateException(
            "A keep attribute expression has to keep at least one attribute"));
  }
}
