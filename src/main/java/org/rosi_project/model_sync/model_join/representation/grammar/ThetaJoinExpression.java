package org.rosi_project.model_sync.model_join.representation.grammar;

import java.util.List;
import javax.annotation.Nonnull;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.core.OCLConstraint;

/**
 * A {@code theta join} is the most general type of join. It will determine join-compatibility
 * based on an arbitrary predicate which may evaluate to {@code true} or {@code false}, denoting
 * whether two instances are to be joined or not.
 * <p>
 * To specify this condition, {@link OCLConstraint OCL} is used.
 * <p>
 * As a simple example, consider the following two classes:
 * <pre>
 * ┌───────────────────┐     ┌───────────────────┐
 * │ Person            │     │ Employee          │
 * ├───────────────────┤     ├───────────────────┤
 * │firstname: String  │     │id: int            │
 * │lastname: String   │     │fullname: String   │
 * │dateOfBirth: Date  │     │dateOfBirth: Date  │
 * │gender: Char       │     │salary: int        │
 * ├───────────────────┤     ├───────────────────┤
 * │ ...               │     │ ...               │
 * └───────────────────┘     └───────────────────┘
 * </pre>
 * <p>
 * The expression {@code person.firstname + ' ' + person.lastname = employee.fullname} would than
 * match all persons with employee instances if they have the same name.
 *
 * @author Rico Bergmann
 */
public class ThetaJoinExpression extends JoinExpression {

  @Nonnull
  private final OCLConstraint condition;

  /**
   * Full constructor.
   *
   * @param left the left class to use in the join
   * @param right the right class to use in the join
   * @param target the name of the resulting view
   * @param condition the condition which should be used to determine join-compatibility
   */
  public ThetaJoinExpression(
      @Nonnull ClassResource left,
      @Nonnull ClassResource right,
      @Nonnull ClassResource target,
      @Nonnull OCLConstraint condition,
      @Nonnull List<KeepExpression> keeps) {
    super(left, right, target, keeps);
    this.condition = condition;
  }

  /**
   * Provides the condition which should be used to determine whether two instances are join
   * compatible or not.
   */
  @Nonnull
  public OCLConstraint getCondition() {
    return condition;
  }

  @Nonnull
  @Override
  public JoinType getType() {
    return JoinType.THETA;
  }

  @Nonnull
  @Override
  String getJoinTypeAsString() {
    return "theta join";
  }
}
