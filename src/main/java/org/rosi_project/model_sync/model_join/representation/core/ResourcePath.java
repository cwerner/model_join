package org.rosi_project.model_sync.model_join.representation.core;

import java.io.File;
import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * A {@code ResourcePath} represents a fully-qualified name of some model element to be used in a
 * join.
 *
 * @author Rico Bergmann
 */
public class ResourcePath {

  /**
   * Generates a new {@code path} given the path to its containing <em>package</em> and the name of
   * the resource which should be referenced.
   */
  @Nonnull
  public static ResourcePath from(@Nonnull String packagePath, @Nonnull String resourceName) {
    return new ResourcePath(packagePath, resourceName);
  }

  /**
   * Generates a new {@code path} given its fully-qualified name.
   *
   * @param qualifiedResourcePath the path. It is expected to conform to the format {@code
   *     pathToResource.resourceName}
   */
  @Nonnull
  public static ResourcePath parse(@Nonnull String qualifiedResourcePath) {
    if (qualifiedResourcePath.equals(".")) {
      throw new IllegalArgumentException(
          "Not a valid resource path: '" + qualifiedResourcePath + "'");
    }

    int splitPos = qualifiedResourcePath.lastIndexOf(".");

    String packagePath;
    if (splitPos == -1) {
      packagePath = "";
    } else {
      packagePath = qualifiedResourcePath.substring(0, splitPos);
    }

    String resourceName = qualifiedResourcePath.substring(splitPos + 1);
    return new ResourcePath(packagePath, resourceName);
  }

  /**
   * Generates a new {@code path} given its fully-qualified name.
   * <p>
   * The name will be interpreted as some location on the file system (that is as {@code
   * /path/to/resource}). However, both {@code \} as well as {@code /} will be accepted as path
   * separators.
   *
   * @param qualifiedResourcePath the path. It is expected to conform to the format {@code
   *     pathToResource/resourceName}
   */
  public static ResourcePath parseAsFileSystemPath(@Nonnull String qualifiedResourcePath) {
    return parse(toPackageStylePath(qualifiedResourcePath));
  }

  /**
   * Converts a path as used by the file system to the corresponding path as used in a Java package.
   */
  protected static String toPackageStylePath(@Nonnull String fileSystemResourcePath) {
    return fileSystemResourcePath.replaceAll("[\\\\/]", ".");
  }

  @Nonnull
  protected final String resourcePath;

  @Nonnull
  protected final String resourceName;

  /**
   * Full constructor.
   *
   * @param resourcePath the path to the directory/package in which the resource resides
   * @param resourceName the name of the class (or other resource)
   */
  public ResourcePath(@Nonnull String resourcePath, @Nonnull String resourceName) {
    if (resourceName.isEmpty()) {
      throw new IllegalArgumentException("Resource name may not be empty");
    }
    this.resourcePath = resourcePath;
    this.resourceName = resourceName;
  }

  /**
   * Provides the path to the directory/package in which {@code this} resource resides. The path
   * will be in "Java-package style", i.e. its components will be separated by {@code .}.
   */
  @Nonnull
  public String getResourcePath() {
    return resourcePath;
  }

  /**
   * Provides the name of the resource which is represented by {@code this} path.
   * <p>
   * The name is relative to the containing directory/package.
   */
  @Nonnull
  public String getResourceName() {
    return resourceName;
  }

  /**
   * Converts {@code this} path to {@code String}. It will match the format {@code package.name}.
   */
  @Nonnull
  public String get() {
    return this.toString();
  }

  /**
   * Converts {@code this} path to a {@code String}, treating it as some path on the file system.
   * <p>
   * The components of the path will be separated by the platform-dependent separator for the
   * current platform. That is, its result may vary depending on the OS on which it is run.
   */
  @Nonnull
  public String getAsFileSystemPath() {
    return this.toString().replaceAll("\\.", File.pathSeparator);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourcePath that = (ResourcePath) o;
    return this.resourcePath.equals(that.resourcePath) &&
        this.resourceName.equals(that.resourceName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(resourcePath, resourceName);
  }

  @Override
  public String toString() {
    if (resourcePath.isEmpty()) {
      return resourceName;
    }
    return resourcePath + "." + resourceName;
  }
}
