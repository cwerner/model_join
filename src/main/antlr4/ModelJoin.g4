/*
 * ModelJoin.g4
 *
 * Defines the structure of our ModelJoin grammar
 *
 * Author: Rico Bergmann
 */
grammar ModelJoin;

/* @header {
package org.rosi_project.model_sync.model_join.representation.parser.antlr.generated;
}*/

/*
 * Parser
 */
modeljoin : join+ EOF ;
join : (naturaljoin | thetajoin | outerjoin) AS classres
  ( OPENCURLYBRAKET
    keepattributesexpr*
    keepaggregatesexpr*
    keepcalculatedexpr*
    keepexpr*
  CLOSEDCURLYBRAKET )? ;
naturaljoin : NATURAL JOIN classres WITH classres ;
thetajoin : THETA JOIN classres WITH classres WHERE oclcond;
outerjoin : (leftouterjoin | rightouterjoin) OUTER JOIN classres WITH classres;
leftouterjoin : LEFT ;
rightouterjoin : RIGHT ;

keepattributesexpr : KEEP ATTRIBUTES attrres (COMMA attrres)* ;
keepaggregatesexpr : KEEP AGGREGATE aggrtype
                      OPENBRAKET relattr CLOSEDBRAKET
                      OVER classres AS attrres  ;
keepcalculatedexpr  : KEEP CALCULATED ATTRIBUTE oclcond AS (typedattrres | attrres) ;
keepexpr : (keeptypeexpr | keepoutgoingexpr | keepincomingexpr)
  ( OPENCURLYBRAKET
    keepattributesexpr*
    keepaggregatesexpr*
    keepcalculatedexpr*
    keepexpr*
  CLOSEDCURLYBRAKET)? ;
keeptypeexpr : keepsupertypeexpr | keepsubtypeexpr ;
keepsupertypeexpr : KEEP SUPERTYPE classres ( AS TYPE classres )? ;
keepsubtypeexpr : KEEP SUBTYPE classres ( AS TYPE classres )? ;
keepoutgoingexpr : KEEP OUTGOING attrres ( AS TYPE classres )? ;
keepincomingexpr : KEEP INCOMING attrres (AS TYPE classres )? ;

classres : WORD (DOT WORD)* ;
attrres : WORD (DOT WORD)+ ;
typedattrres  : attrres COLON WORD  ;
relattr : WORD ;
oclcond : (WORD | NUMBER | specialchar | anytoken | WHITESPACE | NEWLINE)+;
aggrtype : SUM | AVG | MIN | MAX | SIZE ;

specialchar : DOT
              | OPENBRAKET
              | CLOSEDBRAKET
              | OPENCURLYBRAKET
              | CLOSEDCURLYBRAKET
              | COMMA
              | UNDERSCORE
              | SPECIALCHAR ;

// anytoken matches all possible tokens in case their content may appear as part of some regular
// text as well
anytoken  : OPENBRAKET
          | CLOSEDBRAKET
          | OPENCURLYBRAKET
          | CLOSEDCURLYBRAKET
          | DOT
          | COLON
          | COMMA
          | UNDERSCORE
          | SPECIALCHAR

          | JOIN
          | NATURAL
          | THETA
          | WHERE
          | OUTER
          | RIGHT
          | LEFT
          | WITH
          | AS

          | KEEP
          | ATTRIBUTES
          | ATTRIBUTE
          | AGGREGATE
          | CALCULATED
          | SUPERTYPE
          | SUBTYPE
          | OUTGOING
          | INCOMING
          | TYPE
          | OVER

          | SUM
          | AVG
          | MIN
          | MAX
          | SIZE ;

/*
 * Lexer
 */

fragment LOWERCASE  : [a-z]                 ;
fragment UPPERCASE  : [A-Z]                 ;
fragment ANYCASE    : LOWERCASE | UPPERCASE ;
fragment DIGIT      : [0-9]                 ;

OPENBRAKET        : '('         ;
CLOSEDBRAKET      : ')'         ;
OPENCURLYBRAKET   : '{'         ;
CLOSEDCURLYBRAKET : '}'         ;
DOT               : '.'         ;
COLON             : ':'         ;
COMMA             : ','         ;
UNDERSCORE        : '_'         ;
SPECIALCHAR       : [-><!="'|]  ;

JOIN    : 'join'    ;
NATURAL : 'natural' ;
THETA   : 'theta'   ;
WHERE   : 'where'   ;
OUTER   : 'outer'   ;
RIGHT   : 'right'   ;
LEFT    : 'left'    ;
WITH    : 'with'    ;
AS      : 'as'      ;

KEEP        : 'keep'        ;
ATTRIBUTES  : 'attributes'  ;
ATTRIBUTE   : 'attribute'   ;
AGGREGATE   : 'aggregate'   ;
CALCULATED  : 'calculated'  ;
SUPERTYPE   : 'supertype'   ;
SUBTYPE     : 'subtype'     ;
OUTGOING    : 'outgoing'    ;
INCOMING    : 'incoming'    ;
TYPE        : 'type'        ;
OVER        : 'over'        ;

SUM   : 'sum'   ;
AVG   : 'avg'   ;
MIN   : 'min'   ;
MAX   : 'max'   ;
SIZE  : 'size'  ;

WORD    : ANYCASE (ANYCASE | DIGIT | UNDERSCORE)* ;
NUMBER  : [+-]? DIGIT+ DOT? DIGIT* ;

WHITESPACE  : ' ' -> skip                   ;
NEWLINE     : ('\r'? '\n' | '\r')+ -> skip  ;
