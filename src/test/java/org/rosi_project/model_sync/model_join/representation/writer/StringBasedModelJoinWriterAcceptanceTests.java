package org.rosi_project.model_sync.model_join.representation.writer;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.logging.Logger;
import org.junit.jupiter.api.Test;
import org.rosi_project.model_sync.model_join.representation.writer.TestedModels.Listing2;

class StringBasedModelJoinWriterAcceptanceTests {

	private static final Logger log =
			Logger.getLogger(StringBasedModelJoinWriterAcceptanceTests.class.getSimpleName());

	/**
	 * @see TestedModels
	 */
	@Test
	void writeProducesListing2FromTechnicalReport() {
		StringBasedModelJoinWriter writer = StringBasedModelJoinWriter.withNewlines();
		String writerOutput = writer.write(Listing2.asModelJoin);

		log.info("Writer output:");
		log.info(writerOutput);

		String sanitizedWriterOutput = sanitize(writerOutput);

		String expectedOutput = sanitize(Listing2.asString);

		// Although AssertJ provides an isEqualToIgnoringWhitespace as well as an
		// isEqualToIgnoringNewline method, there's none that ignores both. Thus we reside to manual
		// sanitization
		assertThat(sanitizedWriterOutput).isEqualTo(expectedOutput);
	}

	private String sanitize(String rawModelJoin) {
		return rawModelJoin.replaceAll("\\s", "");
	}
}
