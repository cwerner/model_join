package org.rosi_project.model_sync.model_join.representation.writer;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.rosi_project.model_sync.model_join.representation.writer.TestedModels.Listing2;

class FileBasedModelJoinWriterAcceptanceTests {

  private static final Logger log = Logger.getLogger(FileBasedModelJoinWriterAcceptanceTests.class.getSimpleName());

  private List<File> createdFiles = new ArrayList<>();

  @AfterEach
  void tearDown() {
    createdFiles.forEach(file -> {
      boolean result = file.delete();
      log.info(String.format("Deleting file %s, successfull=%s", file, result));
    });
    createdFiles.clear();
  }

  /**
   * Depends on {@link StringBasedModelJoinWriterAcceptanceTests#writeProducesListing2FromTechnicalReport()}
   */
  @Test
  void writeProducesListing2FromTechnicalReport() {
    File outputFile = new File("listing2.modeljoin");
    registerCreatedFile(outputFile);

    FileBasedModelJoinWriter writer = new FileBasedModelJoinWriter(outputFile);
    writer.write(Listing2.asModelJoin);

    logFileContent(outputFile);

    String expectedContent = StringBasedModelJoinWriter.withNewlines().write(Listing2.asModelJoin);
    assertThat(outputFile).exists();
    assertThat(outputFile).hasContent(expectedContent);
  }

  private void registerCreatedFile(File newFile) {
    createdFiles.add(newFile);
    log.info(String.format("Created new file %s", newFile));
  }

  private void logFileContent(File file) {
    try {
      String contents = Files.lines(file.toPath()).collect(Collectors.joining());
      log.info(String.format("Content of file %s:", file));
      log.info(contents);
    } catch (IOException e) {
      log.severe(String.format("Could not read file %s", file));
    }
  }

}
