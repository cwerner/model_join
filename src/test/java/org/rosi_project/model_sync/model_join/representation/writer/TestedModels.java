package org.rosi_project.model_sync.model_join.representation.writer;

import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.attributes;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.outgoing;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.supertype;
import static org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder.thetaJoin;
import org.rosi_project.model_sync.model_join.representation.core.AttributePath;
import org.rosi_project.model_sync.model_join.representation.core.ClassResource;
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression;
import org.rosi_project.model_sync.model_join.representation.util.ModelJoinBuilder;

class TestedModels {

  /*
   * The technical report which forms the basis of the tests here is
   *
   * Burger, E. et Al.: "ModelJoin - A Textual Domain-Specific Language for the Combination of
   *    Heterogeneous Models"; Karslruhe Institute of Technololgy 2014
   */

  static class Listing2 {
    static final ClassResource imdbFilm = ClassResource.from("imdb", "Film");
    static final ClassResource libraryVideoCassette = ClassResource.from("library", "VideoCassette");
    static final ClassResource libraryAudioVisualItem = ClassResource.from("library", "AudioVisualItem");
    static final ClassResource jointargetMovie = ClassResource.from("jointarget", "Movie");
    static final ClassResource jointargetVote = ClassResource.from("jointarget", "Vote");
    static final ClassResource jointargetMediaItem = ClassResource.from("jointarget", "MediaItem");

    static final AttributePath imdbFilmYear = AttributePath.from(imdbFilm, "year");
    static final AttributePath imdbFilmVotes = AttributePath.from(imdbFilm, "votes");
    static final AttributePath imdbVoteScore = AttributePath.from("imdb.Vote", "score");
    static final AttributePath libraryAudioVisualItemMinutesLength = AttributePath.from(libraryAudioVisualItem, "minutesLength");

    //static final ThetaJoinBuilder tjb = (ThetaJoinBuilder)JoinFactory.createNew().theta().join(imdbFilm).with(libraryVideoCassette).as(jointargetMovie);    
    static final ModelJoinExpression asModelJoin = ModelJoinBuilder.createNewModelJoin()
        .add(thetaJoin()
        	.where("library.VideoCassette.cast->forAll (p | imdb.Film.figures->playedBy->exists (a | p.firstname.concat(\" \") .concat(p.lastName) == a.name))")
            .join(imdbFilm)
            .with(libraryVideoCassette)
            .as(jointargetMovie)
        	.keep(attributes(imdbFilmYear))
            .keep(outgoing(imdbFilmVotes)
                .as(jointargetVote)
                .keep(attributes(imdbVoteScore))
                .buildExpression()
            )
            .keep(supertype(libraryAudioVisualItem)
                .as(jointargetMediaItem)
                .keep(attributes(libraryAudioVisualItemMinutesLength))
                .buildExpression()
            )
            .done())
        .build();
    /*static final KeepAttributesExpression ke = attributes(imdbFilmYear);
    
    static final ModelJoinExpression asModelJoin = ModelJoinBuilder.createNewModelJoin()
            .add(naturalJoin()
                .join(imdbFilm)
                .with(libraryVideoCassette)
                .as(jointargetMovie)
            	.keep(ke)
                .keep(outgoing(imdbFilmVotes)
                    .as(jointargetVote)
                    .keep(attributes(imdbVoteScore))
                    .buildExpression()
                )
                .keep(supertype(libraryAudioVisualItem)
                    .as(jointargetMediaItem)
                    .keep(attributes(libraryAudioVisualItemMinutesLength))
                    .buildExpression()
                )
                .done())
            .build();*/

    static final String asString = "theta join imdb.Film with library.VideoCassette as jointarget.Movie\n"
        + "where library.VideoCassette.cast->forAll (p | imdb.Film.figures->playedBy->exists (a | p.\n"
        + "firstname.concat(\" \") .concat(p.lastName) == a.name)) {\n"
        + " keep attributes imdb.Film.year\n"
        + " keep outgoing imdb.Film.votes as type jointarget.Vote {\n"
        + " keep attributes imdb.Vote.score\n"
        + "}\n"
        + "keep supertype library.AudioVisualItem as type jointarget.MediaItem {\n"
        + " keep attributes library.AudioVisualItem.minutesLength\n"
        + "}\n"
        + "}";
  }

}
