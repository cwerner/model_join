# ModelJoin

This repository contains an Antlr Grammar of the **ModelJoin** Language from [Burger 2014, Burger 2016] for view type generation. It is used in the Role-based Single Underlying Model (**RSUM**) approach to describe queries that are used for view type generation. In addition, the repository contains tests and model join queries for testing. The tests using a library example with three different models.

- **Burger 2014**: E. Burger, J. Henß, S. Kruse, M. Küster, A. Rentschler, and L. Happe, ModelJoin: A Textual Domain-Specific Language for the Combination of Heterogeneous Models. KIT, Fakultät für Informatik, 2014
- **Burger 2016**: E. Burger, J. Henss, M. Küster, S. Kruse, and L. Happe, View-Based Model-Driven Software Development with ModelJoin. Software & Systems Modeling, 2016 , volume 15, pages 473-496 
